Gestor de Capcacitaciones V1
============================

El "Gestor de Capcacitaciones V1" es una aplicación propia y para el
uso de Capiro S.A. para el manejo y adminsitración de capacitaciones.

Requisitos
------------

  * PHP 7.1.3 o superior;
  * PDO-SQLite Extensión de PHP activa;
  * MySQl o MariaDB.

Instalación
------------

Copiar archivos al servidor y configurar las medidas de acceso de la aplicación
como tambien url base e información perteneciente a la base de datos en.

``config/app.json``
``config/db.json``

Sistema de archivos y navegación
---------------------------------
--app "Logica propia del proyecto independiente de la estructura"
    --Controllers "Capa intermedia entre la vista y el modelo, encargado de las validaciones y procesamiento de la información"
    --Documents "Documentos propios de la aplicación"
    --Models "Agrupacion de todas la consultas sql en clases"
    --Views "Diseño de la aplicación"

--app-public "Archivos de acceso publico"
    --assets
        --css "Estilos"
        --images "imagenes de la aplicación"
        --js "Archivos JavaScript"

--bin
    --Connections "Conexiones a otras base de datos o sistemas"
    --Dictionaries "Diccionarios con permisos e información considerada cache"
    --Functions "Funciones de ayuda global"
    --SystemClass "Clases de la aplicación no pertenecientes a controladores"

--config "Configuraciones de la aplicación"

--core "Clases propias de la estructura 'mini-framework'"

--vendor "Extensiones"
