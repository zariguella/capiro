<?php

class RosterModel extends RosterConn
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll(): array
    {
        $pst = $this->conn->prepare(
            "SELECT --*
 --/*
                 CON.c0550_rowid_tercero thirdTableId,
                 CON.c0550_rowid registryId,
                 TER.f200_nombres name,
                 CONCAT(TER.f200_apellido1, ' ', TER.f200_apellido2) lastName,
                 CONCAT(TER.f200_apellido1, ' ', TER.f200_apellido2,' ',TER.f200_nombres) fullName,
                 TER.f200_nit code,
                 TER.f200_id_tipo_ident codeType,
                 CON.c0550_id_co pCenterCode,
                 CC.F284_ID cCenterCode,
                 f284_descripcion costCenter,
                 CO.f285_descripcion productionCenter,
                 CO.f285_id prodCenter,
                 CC.f284_id_grupo_ccosto Area,
                 CASE WHEN  EM.c0540_ind_sexo=0 THEN 'MASCULINO'
                 WHEN EM.c0540_ind_sexo=1 THEN 'FEMENINO'
                 END sex,
                 --*/
                CASE WHEN CC.f284_id_grupo_ccosto IN ('CI','CA','CV') THEN 'ADMINISTRATIVOS'
                WHEN CC.f284_id_grupo_ccosto IN ('CD') THEN 'OPERATIVOS'
                END SELECCION_MACRO
                 
                FROM w0550_contratos CON
                INNER JOIN t200_mm_terceros TER ON CON.c0550_rowid_tercero = TER.f200_rowid AND 
                  TER.f200_id_cia = CON.c0550_id_cia
                INNER JOIN t284_co_ccosto CC    ON CON.c0550_rowid_ccosto = CC.f284_rowid
                 
                INNER JOIN t285_co_centro_op CO ON CO.f285_id= c0550_id_co
                 
                INNER JOIN w0540_empleados EM ON EM.c0540_rowid_tercero = CON.c0550_rowid_tercero
                 
                WHERE CON.c0550_ind_estado = 1
                ORDER BY lastName ASC"
        );
        $pst->execute();
        return $pst->fetchAll();
        /*SELECT --*
                 --/*
                 CON.c0550_rowid_tercero thirdTableId,
                 CON.c0550_rowid registryId,
                 TER.f200_nombres name,
                 CONCAT(TER.f200_apellido1, ' ', TER.f200_apellido2) lastName,
                 CONCAT(TER.f200_nombres, ' ', TER.f200_apellido1, ' ', TER.f200_apellido2) fullName,
                 TER.f200_nit code,
                 TER.f200_id_tipo_ident codeType,
                 CON.c0550_id_co pCenterCode,
                 CC.F284_ID cCenterCode,
                 f284_descripcion costCenter,
                 CO.f285_descripcion productionCenter,
                 CASE WHEN  EM.c0540_ind_sexo=0 THEN 'MASCULINO'
                 WHEN EM.c0540_ind_sexo=1 THEN 'FEMENINO'
                 END sex
                 --*/
               /* FROM w0550_contratos CON
                INNER JOIN t200_mm_terceros TER ON CON.c0550_rowid_tercero = TER.f200_rowid AND
                  TER.f200_id_cia = CON.c0550_id_cia
                INNER JOIN t284_co_ccosto CC    ON CON.c0550_rowid_ccosto = CC.f284_rowid
                INNER JOIN t285_co_centro_op CO ON CO.f285_id= c0550_id_co
                INNER JOIN w0540_empleados EM ON EM.c0540_rowid_tercero = CON.c0550_rowid_tercero*/
    }

    public function getCostCenter($codigo): array
    {
        $pst = $this->conn->prepare(
                "SELECT DISTINCT
               -- CC.F284_ID costCenter,
                CC.f284_descripcion costCenterName,              
                CC.F284_ID costCenter
                FROM w0550_contratos CON
                INNER JOIN t284_co_ccosto CC ON CON.c0550_rowid_ccosto = CC.f284_rowid
                INNER JOIN t285_co_centro_op CO ON CO.f285_id= CON.c0550_id_co
                WHERE CON.c0550_ind_estado = 1  AND CON.c0550_id_co = :codigo
                ORDER BY 1 ASC"

               /* "SELECT     --*
                CC.F284_ID costCenter,
                f284_descripcion costCenterName
                FROM t284_co_ccosto CC
                WHERE f284_id_un='001'
                ORDER BY f284_descripcion ASC"

                -------- CONSULTA ADMINISTRATIVOS---------

                SELECT DISTINCT
               -- CC.F284_ID costCenter,
                CC.f284_descripcion costCenterName,              
                CC.F284_ID costCenter,
                CASE WHEN CC.f284_id_grupo_ccosto IN ('CI','CA','CV') THEN 'ADMINISTRATIVOS'
                WHEN CC.f284_id_grupo_ccosto IN ('CD')THEN 'OPERATIVOS'
                END SELECCION_MACRO
                FROM w0550_contratos CON
                INNER JOIN t284_co_ccosto CC ON CON.c0550_rowid_ccosto = CC.f284_rowid
                INNER JOIN t285_co_centro_op CO ON CO.f285_id= CON.c0550_id_co
                WHERE CON.c0550_ind_estado = 1  
                ORDER BY 1 ASC*/
        );
        $pst->bindParam(":codigo", $codigo);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getProductionCenter(): array
    {
        $pst = $this->conn->prepare(
            "SELECT     --*
                CO.f285_descripcion productionCenterName,
                CO.f285_id productionCenter
            FROM t285_co_centro_op CO
                ORDER BY f285_descripcion ASC"
        );
        $pst->execute();
        return $pst->fetchAll();
    }
    public function getAdministrative(): array{
        $pst = $this->conn->prepare(
            "SELECT DISTINCT
                CASE WHEN CC.f284_id_grupo_ccosto IN ('CI','CA','CV') THEN 'ADMINISTRATIVOS'
                WHEN CC.f284_id_grupo_ccosto IN ('CD') THEN 'OPERATIVOS'
                END SELECCION_MACRO
                FROM w0550_contratos CON
                INNER JOIN t284_co_ccosto CC ON CON.c0550_rowid_ccosto = CC.f284_rowid
                INNER JOIN t285_co_centro_op CO ON CO.f285_id= CON.c0550_id_co
                WHERE CON.c0550_ind_estado = 1 
                ORDER BY 1 ASC"
        );
        $pst->execute();
        return $pst->fetchAll();
    }
}
