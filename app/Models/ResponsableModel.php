<?php

class ResponsableModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "responsable";
        $this->fields = ["code" => "resp_code", "name" => "resp_name", "state" => "resp_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT resp_code
                FROM responsable ORDER BY resp_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO responsable ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE responsable SET {$string}
                WHERE resp_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM responsable
                WHERE resp_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT resp_code 'code', resp_name 'name'
                FROM responsable
                WHERE resp_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT resp_code 'code', resp_name 'name', resp_state 'state'
                FROM responsable;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT resp_code 'code', resp_name 'name', resp_state 'state'
                FROM responsable
                WHERE resp_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT resp_code 'code'
                FROM responsable
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
