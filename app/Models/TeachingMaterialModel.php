<?php

class TeachingMaterialModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "teachingmaterial";
        $this->fields = ["code" => "teacmate_code", "name" => "teacmate_name", "description" => "teacmate_description", "state" => "teacmate_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT teacmate_code
                FROM teachingmaterial ORDER BY teacmate_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO teachingmaterial ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE teachingmaterial SET {$string}
                WHERE teacmate_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM teachingmaterial
                WHERE teacmate_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT teacmate_code 'code', teacmate_name 'name', teacmate_description 'description', ('') 'selected', ('') 'amount'
                FROM teachingmaterial
                WHERE teacmate_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT teacmate_code 'code', teacmate_name 'name', teacmate_description 'description', teacmate_state 'state'
                FROM teachingmaterial;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT teacmate_code 'code', teacmate_name 'name', teacmate_description 'description', teacmate_state 'state'
                FROM teachingmaterial
                WHERE teacmate_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT teacmate_code 'code'
                FROM teachingmaterial
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
