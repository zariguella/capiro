<?php

class TestModel extends Model
{

    public function hola(int $code): array
    {
        $query = $this->conn->prepare(
            "SELECT plac_code 'code', plac_name 'name', plac_state 'state' FROM place WHERE place.plac_code = :code"
        );
        $query->bindParam(":code", $code, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }
}
