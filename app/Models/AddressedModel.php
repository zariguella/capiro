<?php

class AddressedModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "addressed";
        $this->fields = ["code" => "addr_code", "name" => "addr_name", "state" => "addr_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT addr_code
                FROM addressed ORDER BY addr_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO addressed ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE addressed SET {$string}
                WHERE addr_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM addressed
                WHERE addr_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT addr_code 'code', addr_name 'name'
                FROM addressed
                WHERE addr_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT addr_code 'code', addr_name 'name', addr_state 'state'
                FROM addressed;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT addr_code 'code', addr_name 'name', addr_state 'state'
                FROM addressed
                WHERE addr_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT addr_code 'code'
                FROM addressed
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
