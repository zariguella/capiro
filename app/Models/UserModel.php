<?php

class UserModel extends Model
{

    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "user";
        $this->fields = [
            "nick" => "user_nick",
            "code" => "user_code",
            "productionCenter" => "user_production_center",
            "state" => "user_state",
            "type" => "user_type"
        ];
    }

    public function create(array $data): bool
   {
       $this->linkParams($data, $this->fields);
       $string = $this->getStrForCreate($data);
       $pst = $this->conn->prepare(
           "INSERT INTO user ({$string['fields']})
            VALUES ({$string['params']});"
        );
       foreach ($data as $key => $value) {
           $pst->bindValue(":{$key}", $data[$key]);
       }

       $success = $pst->execute();
       if ($success) {
           $audit  = new Audit();
           if (!$audit->create("INSERT", "user", $data)) {
               endAjax("warning", "Error en auditoria: " . $audit->getErrors());
           }
       }
       return $success;
   }

    public function update($code, array $data): bool
    {
        $oldData = $this->getByCode($code)[0];

        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE user SET {$string}
            WHERE user_code = :code LIMIT 1;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_STR);

        $success = $pst->execute();
        if ($success) {
            $audit  = new Audit();
            $data["user_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "user", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function updateProductionCenter(string $code, int $productionCenter): bool
    {
        $pst = $this->conn->prepare(
            "UPDATE user SET user_production_center = :productionCenter
            WHERE user_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        $pst->bindParam(":productionCenter", $productionCenter, PDO::PARAM_INT);

        return $pst->execute();
    }

    public function delete(string $code): bool
    {
        $oldData = $this->getByCode($code)[0];

        $pst = $this->conn->prepare(
            "DELETE FROM user WHERE user_code = :code;"
        );
        $pst->bindValue(":code", $code);

        $success = $pst->execute();
        if ($success) {
            $audit  = new Audit();
            $data["user_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("DELETE", "user", [], $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function getUsers(): array
    {
        $query = $this->conn->query(
            "SELECT user.user_code 'code', usertype.usertype_name 'typeName', user.user_state 'state', user.user_type 'type'
                FROM user INNER JOIN usertype
                ON user.user_type = usertype.usertype_code;"
        );

		return $query->fetchAll();
    }

    public function getByCode(string $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT user.user_code 'code', user.user_nick 'nick', user.user_state 'state', user.user_type 'type', usertype.usertype_name 'typeName'
                FROM {$this->tblName} INNER JOIN usertype
                ON user.user_type = usertype.usertype_code
                WHERE user.user_code='{$code}';");
        $pst->execute();
		return $pst->fetchAll();
    }

    public function getByCodeOrNick(array $data): string
    {
        $pst = $this->conn->prepare(
            "SELECT user_code 'code'
                FROM user
                WHERE user_nick = :nick OR user_code = :code
                LIMIT 1;"
        );
        $pst->bindParam(":nick", $data["nick"], PDO::PARAM_STR);
        $pst->bindParam(":code", $data["code"], PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function getCodeByNick(string $nick): string
    {
        $pst = $this->conn->prepare(
            "SELECT user_code 'code'
                FROM user
                WHERE user_nick = :nick
                LIMIT 1;"
        );
        $pst->bindParam(":nick", $nick, PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function getUserType(): array
    {
        $query = $this->conn->query(
            "SELECT usertype_code 'code', usertype_name 'type' FROM usertype;"
        );
        return $query->fetchAll();
    }

    public function getNotifications(string $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT nt.nt_code 'code', nt.nt_training_code 'trainingCode', tr.trai_date_format 'trainingDate',
                    nt.nt_date 'date', th.them_name 'theme', ntt.ntt_message 'message', ntt.ntt_title 'title'
            	FROM notification nt
                INNER JOIN notification_type ntt ON ntt.ntt_code = nt.nt_type
                INNER JOIN training tr ON tr.trai_code = nt.nt_training_code
                INNER JOIN theme th ON th.them_code = tr.trai_theme
                WHERE nt.nt_user_code = :code AND nt.nt_state = 1 ORDER BY nt.nt_date DESC;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getAllNotifications(string $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT nt.nt_code 'code', nt.nt_training_code 'trainingCode', tr.trai_date_format 'trainingDate',
                    nt.nt_date 'date', th.them_name 'theme', ntt.ntt_message 'message', ntt.ntt_title 'title'
            	FROM notification nt
                INNER JOIN notification_type ntt ON ntt.ntt_code = nt.nt_type
                INNER JOIN training tr ON tr.trai_code = nt.nt_training_code
                INNER JOIN theme th ON th.them_code = tr.trai_theme
                WHERE nt.nt_user_code = :code ORDER BY nt.nt_date DESC LIMIT 50;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function clearNotifications($code): bool
    {
        $pst = $this->conn->prepare(
            "UPDATE notification
                SET nt_state = 0
                WHERE nt_user_code = :code AND nt_state = 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        return $pst->execute();
    }
}
