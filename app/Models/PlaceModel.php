<?php

class PlaceModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "place";
        $this->fields = ["code" => "plac_code", "name" => "plac_name", "state" => "plac_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT plac_code
                FROM place ORDER BY plac_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO place ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE place SET {$string}
                WHERE plac_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM place
                WHERE plac_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT plac_code 'code', plac_name 'name'
                FROM place
                WHERE plac_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT plac_code 'code', plac_name 'name', plac_state 'state'
                FROM place;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT plac_code 'code', plac_name 'name', plac_state 'state'
                FROM place
                WHERE plac_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT plac_code 'code'
                FROM place
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
