<?php

class ProductionCenterModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "productioncenter";
        $this->fields = ["code" => "prodcent_code", "name" => "prodcent_name", "abbreviation" => "prodcent_abbreviation", "state" => "prodcent_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT prodcent_code
                FROM productioncenter ORDER BY prodcent_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO productioncenter ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE productioncenter SET {$string}
                WHERE prodcent_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM productioncenter
                WHERE prodcent_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT prodcent_code 'code', prodcent_name 'name', prodcent_abbreviation 'abbreviation'
                FROM productioncenter
                WHERE prodcent_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT prodcent_code 'code', prodcent_name 'name', prodcent_abbreviation 'abbreviation', prodcent_state 'state'
                FROM productioncenter;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT prodcent_code 'code', prodcent_name 'name', prodcent_abbreviation 'abbreviation', prodcent_state 'state'
                FROM productioncenter
                WHERE prodcent_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByName(string $name): array
    {
        $pst = $this->conn->prepare(
            "SELECT prodcent_code 'code', prodcent_name 'name', prodcent_abbreviation 'abbreviation', prodcent_state 'state'
                FROM productioncenter
                WHERE prodcent_name LIKE :name LIMIT 1;"
        );
        $name =  '%' . $name . '%';
        $pst->bindParam(":name", $name, PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT prodcent_code 'code'
                FROM productioncenter
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
