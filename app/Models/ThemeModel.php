<?php

class ThemeModel extends Model
{
    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "theme";
        $this->fields = ["code" => "them_code", "name" => "them_name", "content" => "them_content", "objective" => "them_objective", "state" => "them_state"];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT them_code
                FROM theme ORDER BY them_code DESC LIMIT 1;"
        );
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO theme ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function update(int $code, array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE theme SET {$string}
                WHERE them_code = :code;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }

    public function delete(int $code): bool
	{
		$pst = $this->conn->prepare(
            "DELETE FROM theme
                WHERE them_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
	}

    public function get(): array
    {
        $query = $this->conn->query(
            "SELECT them_code 'code', them_name 'name', them_content 'content', them_objective 'objective'
                FROM theme
                WHERE them_state = 1;"
        );
        return $query->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT them_code 'code', them_name 'name', them_content 'content', them_objective 'objective', them_state 'state'
                FROM theme;"
        );
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT them_code 'code', them_name 'name', them_state 'state', them_content 'content', them_objective 'objective'
                FROM theme
                WHERE them_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getBy($by, $value): int
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT them_code 'code'
                FROM theme
                WHERE {$by} = :value LIMIT 1;"
        );
        $pst->bindParam(":value", $value);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
