<?php

class AuditModel extends Model
{

    private $tblName, $fields;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "audit";
        $this->fields = [
            "code" => "audi_code",
            "user" => "audi_user",
            "date" => "audi_date",
            "type" => "audi_type",
            "table" => "audi_table",
            "inserted" => "audi_inserted",
            "deleted" => "audi_deleted"
        ];
    }

    public function getLastCode(): int
    {
        $query = $this->conn->query("SELECT audi_code FROM audit ORDER BY audi_code DESC LIMIT 1;");
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function getDateTime(): string
    {
        $query = $this->conn->query("SELECT NOW();");
        return $query->fetchColumn();
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO audit ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        return $pst->execute();
    }

    public function getLastSignin(int $code, int $type): string
    {
        $pst = $this->conn->prepare(
            "SELECT audi_date 'date'
                FROM audit
                WHERE audi_user = :code AND audi_type = :type LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        $pst->bindParam(":type", $type, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchColumn();
    }
}
