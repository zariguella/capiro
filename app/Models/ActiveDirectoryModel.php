<?php

class ActiveDirectoryModel extends ActiveDirectoryConn
{

    public function validateUser(string $nick, string $pass): bool
    {
        return (bool) $this->bind($nick, $pass);
    }
}
