<?php

class TrainingModel extends Model
{
    private $tblName, $fields, $tblProcessName, $tblThemeName, $tblProductionCenterName;

    public function __construct()
    {
        parent::__construct();
        $this->tblName = "training";
        $this->fields = [
            "code" => "trai_code",
            "state" => "trai_state",
            "targetPeople" => "trai_target_people",
            "year" => "trai_date_year",
            "week" => "trai_date_week",
            "date" => "trai_date_format",
            "productionCenter" => "trai_production_center",
            "process" => "trai_process",
            "theme" => "trai_theme",
            "plannedHours" => "trai_planned_hours",
            "addressed" => "trai_addressed",
            "responsable" => "trai_responsable",
            "place" => "trai_place",
            "percentEvaluate" => "trai_percent_evaluate",
            "startTime" => "trai_start_time",
            "endTime" => "trai_end_time",
            "responsableName" => "trai_responsable_name",
            "trainerName" => "trai_trainer_name",
            "trainerPosition" => "trai_trainer_position",
            "az" => "trai_az_cd"
        ];

        $this->tblTrainingStateName = "trainingstate";
        $this->trainingStateFields = ["code" => "traistat_code", "name" => "traistat_name"];

        $this->tblProcessName = "process";
        $this->processFields = ["code" => "proc_code", "name" => "proc_name", "abbreviation" => "proc_abbreviation", "state" => "proc_state"];

        $this->tblThemeName = "theme";
        $this->themeFields = ["code" => "them_code", "name" => "them_name", "content" => "them_content", "objective" => "them_objective", "state" => "them_state"];

        $this->tblProductionCenterName = "productioncenter";
        $this->productionCenterFields = ["code" => "prodcent_code", "name" => "prodcent_name", "abbreviation" => "prodcent_abbreviation", "state" => "prodcent_state"];

        $this->tblPlaceName = "place";
        $this->placeFields = ["code" => "plac_code", "name" => "plac_name", "state" => "plac_state"];

        $this->tblAddressedName = "addressed";
        $this->addressedFields = ["code" => "addr_code", "name" => "addr_name", "state" => "addr_state"];

    }

    public function getLastCode(): int
    {
        $query = $this->conn->query(
            "SELECT trai_code
                FROM training
                ORDER BY trai_code DESC LIMIT 1;
        ");
        $lastCode = $query->fetchColumn();
        if ($lastCode) {
            return $lastCode;
        }else {
            return 0;
        }
    }

    public function create(array $data): bool
    {
        $this->linkParams($data, $this->fields);
        $string = $this->getStrForCreate($data);
        $pst = $this->conn->prepare(
            "INSERT INTO training ({$string['fields']})
                VALUES ({$string['params']});"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }

        $success = $pst->execute();
        if ($success) {
            $audit  = new Audit();
            if (!$audit->create("INSERT", "training", $data)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function updatePlanned(int $code, array $data): bool
    {
        $oldData = $this->getByCode($code)[0];

        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE training
                SET {$string}
                WHERE trai_code = :code AND trai_state = 1;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "training", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function updateState(int $code, int $state): bool
    {
        $oldData = $this->getByCode($code)[0];

        $pst = $this->conn->prepare(
            "UPDATE training
                SET trai_state = :state
                WHERE trai_code = :code;"
        );
        $pst->bindParam(":state", $state, PDO::PARAM_INT);
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $data["trai_state"] = $state;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "training", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function updateInExecution(int $code, array $data): bool
    {
        $oldData = $this->getByCode($code)[0];

        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE training
                SET {$string}
                WHERE trai_code = :code AND trai_state = 2;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "training", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function updateExecuted(int $code, array $data): bool
    {
        $oldData = $this->getByCode($code)[0];

        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE training
                SET {$string}
                WHERE trai_code = :code AND trai_state = 2;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "training", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function endUp(int $code, array $data): bool
    {
        $oldData = $this->getByCode($code)[0];

        $this->linkParams($data, $this->fields);
        $string = $this->getStrForUpdate($data);
        $pst = $this->conn->prepare(
            "UPDATE training
                SET {$string}
                WHERE trai_code = :code AND trai_state = 2;"
        );
        foreach ($data as $key => $value) {
            $pst->bindValue(":{$key}", $data[$key]);
        }
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("UPDATE", "training", $data, $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
    }

    public function delete(int $code): bool
	{
        $oldData = $this->getByCode($code)[0];

		$pst = $this->conn->prepare(
            "DELETE FROM training
                WHERE trai_code = :code AND trai_state = 1 LIMIT 1;"
        );
		$pst->bindParam(":code", $code, PDO::PARAM_INT);
        $success = $pst->execute();

        if ($success) {
            $audit  = new Audit();
            $data["trai_code"] = $code;
            $this->linkParams($oldData, $this->fields);
            if (!$audit->create("DELETE", "training", [], $oldData)) {
                endAjax("warning", "Error en auditoria: " . $audit->getErrors());
            }
        }
        return $success;
	}

    public function getListInExecution(): array
    {
        $query = $this->conn->query(
            "SELECT
                training.trai_code 'code',
                training.trai_state 'state',
                training.trai_date_week 'week',
                process.proc_name 'process',
                theme.them_name 'theme',
                productioncenter.prodcent_name 'productionCenterName',
                training.trai_production_center 'productionCenter'
            FROM training
            INNER JOIN  productioncenter ON training.trai_production_center = productioncenter.prodcent_code
            INNER JOIN theme ON training.trai_theme = theme.them_code
            INNER JOIN process ON training.trai_process = process.proc_code
            WHERE training.trai_state = 2;"
        );
        return $query->fetchAll();
    }

    public function getPlanned(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                trai_code 'code',
                trai_date_week 'week',
                trai_date_format 'date',
                trai_date_year 'year',
                trai_production_center 'productionCenter',
                trai_process 'process',
                trai_theme 'theme',
                trai_target_people 'targetPeople',
                trai_responsable 'responsable',
                trai_state 'state',
                trai_addressed 'addressed',
                trai_planned_hours 'plannedHours'
            FROM training
            WHERE trai_code = :code AND trai_state = 1 LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getInExecution(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                trai_code 'code',
                trai_state 'state',
                trai_target_people 'targetPeople',
                trai_date_year 'year',
                trai_date_week 'week',
                trai_date_format 'date',
                trai_production_center 'productionCenter',
                trai_process 'process',
                trai_theme 'theme',
                trai_planned_hours 'plannedHours',
                trai_addressed 'addressed',
                trai_responsable 'responsable',
                trai_place 'place',
                trai_percent_evaluate 'percentEvaluate',
                trai_start_time 'startTime',
                trai_responsable_name 'responsableName',
                trai_trainer_name 'trainerName',
                trai_trainer_position 'trainerPosition',
                trai_az_cd 'az'
            FROM training
            WHERE trai_code = :code AND trai_state = 2 LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getExecuted(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                training.trai_code 'code',
                training.trai_date_format 'date',
                training.trai_target_people 'targetPeople',
                training.trai_planned_hours 'plannedHours',
                training.trai_percent_evaluate 'percentEvaluate',
                training.trai_start_time 'startTime',
                training.trai_end_time 'endTime',
                training.trai_az_cd 'az',
                process.proc_name 'process',
                theme.them_name 'theme',
                productioncenter.prodcent_name 'productionCenter'
            FROM training
            INNER JOIN  productioncenter ON training.trai_production_center = productioncenter.prodcent_code
            INNER JOIN theme ON training.trai_theme = theme.them_code
            INNER JOIN process ON training.trai_process = process.proc_code
            WHERE training.trai_state = 2 AND training.trai_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getAll(): array
    {
        $query = $this->conn->query(
            "SELECT
                trai_code 'code',
                trai_state state
                trai_target_people 'targetPeople',
                trai_date_year 'year',
                trai_date_week 'week',
                trai_date_format 'date',
                trai_production_center 'productionCenter',
                trai_process 'process',
                trai_theme 'theme',
                trai_planned_hours 'plannedHours',
                trai_addressed 'addressed',
                trai_responsable 'responsable',
                trai_place 'place',
                trai_percent_evaluate 'percentEvaluate',
                trai_start_time 'startTime',
                trai_end_time 'endTime',
                trai_responsable_name 'responsableName',
                trai_trainer_name 'trainerName',
                trai_trainer_position 'trainerPosition',
                trai_az_cd 'az'
            FROM training LIMIT 100;
        ");
        return $query->fetchAll();
    }

    public function getByCode(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
               trai_code 'code',
               trai_state 'state',
               trai_target_people 'targetPeople',
               trai_date_year 'year',
               trai_date_week 'week',
               trai_date_format 'date',
               trai_production_center 'productionCenter',
               trai_process 'process',
               trai_theme 'theme',
               trai_planned_hours 'plannedHours',
               trai_addressed 'addressed',
               trai_responsable 'responsable',
               trai_place 'place',
               trai_percent_evaluate 'percentEvaluate',
               trai_start_time 'startTime',
               trai_end_time 'endTime',
               trai_responsable_name 'responsableName',
               trai_trainer_name 'trainerName',
               trai_trainer_position 'trainerPosition',
               trai_az_cd 'az'
            FROM training
            WHERE trai_code = :code LIMIT 1;
        ");
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByYear(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                training.trai_code 'code',
                training.trai_state 'state',
                training.trai_date_year 'year',
                training.trai_date_week 'week',
                training.trai_date_format 'date',
                theme.them_name 'theme',
                trainingstate.traistat_name 'stateName'
            FROM training
            INNER JOIN theme ON training.trai_theme = theme.them_code
            INNER JOIN trainingstate ON training.trai_state = trainingstate.traistat_code
            WHERE training.trai_date_year = :year AND training.trai_production_center = :productionCenter;"
        );
        $pst->bindValue(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindValue(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getToPrint(int $code)
    {
        $pst = $this->conn->prepare(
            "SELECT
                training.trai_code 'code',
                training.trai_date_format 'date',
                training.trai_start_time 'startTime',
                training.trai_planned_hours 'plannedHours',
                training.trai_target_people 'targetPeople',
                productioncenter.prodcent_name 'productionCenter',
                place.plac_name 'place',
                theme.them_name 'theme',
                theme.them_objective 'objective',
                theme.them_content 'content',
                process.proc_name 'process',
                addressed.addr_name 'addressed'
            FROM training
            INNER JOIN  productioncenter ON training.trai_production_center = productioncenter.prodcent_code
            LEFT JOIN  place ON training.trai_place = place.plac_code
            INNER JOIN  addressed ON training.trai_addressed = addressed.addr_code
            INNER JOIN theme ON training.trai_theme = theme.them_code
            INNER JOIN process ON training.trai_process = process.proc_code
            WHERE training.trai_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getToView(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                tr.trai_code 'code',
                tr.trai_state 'state',
                tr.trai_target_people 'targetPeople',
                tr.trai_date_year 'year',
                tr.trai_date_week 'week',
                tr.trai_date_format 'date',
                prc.prodcent_name 'productionCenter',
                pr.proc_name 'process',
                th.them_name 'theme',
                th.them_objective 'objective',
                th.them_content 'content',
                tr.trai_planned_hours 'plannedHours',
                ad.addr_name 'addressed',
                re.resp_name 'responsable',
                pl.plac_name 'place',
                tr.trai_percent_evaluate 'percentEvaluate',
                tr.trai_start_time 'startTime',
                tr.trai_end_time 'endTime',
                tr.trai_responsable_name 'responsableName',
                trp.traiposi_name 'trainerPosition',
                tr.trai_trainer_name  'trainerName',
                tr.trai_az_cd  'az'
            FROM training tr
            LEFT JOIN  productioncenter prc ON tr.trai_production_center = prc.prodcent_code
            LEFT JOIN  place pl ON tr.trai_place = pl.plac_code
            LEFT JOIN  addressed ad ON tr.trai_addressed = ad.addr_code
            LEFT JOIN theme th ON tr.trai_theme = th.them_code
            LEFT JOIN process pr ON tr.trai_process = pr.proc_code
            LEFT JOIN responsable re ON tr.trai_responsable = re.resp_code
            LEFT JOIN trainerposition trp ON tr.trai_trainer_position = trp.traiposi_code
            WHERE tr.trai_code = :code LIMIT 1;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getMaterialsSelected(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                tte.traiteacmate_training_code 'trainingCode',
                tte.traiteacmate_material_code 'materialCode',
                tte.traiteacmate_amount 'amount',
                tem.teacmate_name 'materialName'
            FROM trainingteachingmaterial tte
            INNER JOIN  teachingmaterial tem ON tte.traiteacmate_material_code = tem.teacmate_code
            WHERE tte.traiteacmate_training_code = :code;"
        );
        $pst->bindParam(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getState(): array
    {
        $query = $this->conn->query(
            "SELECT traistat_code 'code', traistat_name 'name'
             FROM trainingstate WHERE traistat_code <> 3;"
        );
        return $query->fetchAll();
    }

    public function getBy(string $by, string $data): bool
    {
        $this->linkParam($by, $this->fields);
        $pst = $this->conn->prepare(
            "SELECT trai_code 'code'
            FROM training
            WHERE {$by}= :data LIMIT 1;
        ");
        $pst->bindValue(":data", $data);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function getAZByCode(int $code): string
    {
        $pst = $this->conn->prepare(
            "SELECT trai_az_cd 'az'
                FROM training
                WHERE trai_code = :code LIMIT 1;
        ");
        $pst->bindValue(":code", $code,  PDO::PARAM_INT);
        $pst->execute();
        $result = $pst->fetchColumn();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    public function getLastAZ(string $az): string
    {
        $pst = $this->conn->prepare(
            "SELECT tr.trai_az_cd 'az'
            	FROM training tr
                INNER JOIN productioncenter prc ON tr.trai_production_center = prc.prodcent_code
                INNER JOIN process pr ON tr.trai_process = pr.proc_code
                WHERE tr.trai_az_cd LIKE ?
                    ORDER BY tr.trai_az_cd DESC LIMIT 1;
        ");
        $pst->bindValue(1, "$az%", PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function getIncompletedAZ(int $code): string
    {
        $pst = $this->conn->prepare(
            "SELECT CONCAT(prc.prodcent_abbreviation , '-', pr.proc_abbreviation) 'az'
            	FROM training tr
                INNER JOIN productioncenter prc ON tr.trai_production_center = prc.prodcent_code
                INNER JOIN process pr ON tr.trai_process = pr.proc_code
                WHERE tr.trai_code = :code LIMIT 1;
        ");
        $pst->bindValue(":code", $code);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function insertAssistants(array $data): bool
    {
        $pst = $this->conn->prepare(
            "INSERT INTO assistant (assi_training_code, assi_employee_code, assi_cost_center)
                VALUES (:training, :employee, :costCenter);"
        );
        $pst->bindValue(":training", $data["training"]);
        $pst->bindValue(":employee", $data["employee"]);
        $pst->bindValue(":costCenter", $data["costCenter"]);
        return $pst->execute();
    }

    public function getTeachingMaterial(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                traiteacmate_material_code 'code',
                traiteacmate_amount  'amount',
                ('true') 'selected'
            FROM trainingteachingmaterial
            WHERE traiteacmate_training_code = :code;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTrainingMaterialByCode(array $data)
    {
        $pst = $this->conn->prepare(
            "SELECT
                traiteacmate_material_code 'code'
            FROM trainingteachingmaterial
            WHERE traiteacmate_training_code = :code AND traiteacmate_material_code = :material;"
        );
        $pst->bindValue(":code", $data["code"], PDO::PARAM_INT);
        $pst->bindValue(":material", $data["material"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchColumn();
    }

    public function updateTrainingMaterial(array $data): bool
    {
        $pst = $this->conn->prepare(
            "UPDATE trainingteachingmaterial
                SET traiteacmate_amount=:amount
                WHERE traiteacmate_training_code = :code AND traiteacmate_material_code = :material;"
        );
        $pst->bindValue(":code", $data["code"], PDO::PARAM_INT);
        $pst->bindValue(":material", $data["material"], PDO::PARAM_INT);
        $pst->bindValue(":amount", $data["amount"], PDO::PARAM_INT);
        return $pst->execute();
    }

    public function createTrainingMaterial(array $data): bool
    {
        $pst = $this->conn->prepare(
            "INSERT INTO trainingteachingmaterial (traiteacmate_training_code, traiteacmate_material_code, traiteacmate_amount)
                VALUES (:code, :material, :amount);"
        );
        $pst->bindValue(":code", $data["code"], PDO::PARAM_INT);
        $pst->bindValue(":material", $data["material"], PDO::PARAM_INT);
        $pst->bindValue(":amount", $data["amount"], PDO::PARAM_INT);
        return $pst->execute();
    }

    public function deleteTrainingMaterial(int $code): bool
    {
        $pst = $this->conn->prepare(
            "DELETE FROM trainingteachingmaterial
                WHERE traiteacmate_training_code = :code;"
        );
		$pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
    }

    public function deleteAssistants(int $code): bool
    {
        $pst = $this->conn->prepare(
            "DELETE FROM assistant
                WHERE assi_training_code = :code;"
        );
		$pst->bindParam(":code", $code, PDO::PARAM_INT);
		return $pst->execute();
    }

    public function createAssistants(array $data): bool
    {
        $pst = $this->conn->prepare(
            "INSERT INTO assistant (assi_training_code, assi_employee_code, assi_cost_center)
                VALUES (:code, :employee, :costCenter);"
        );
        $pst->bindValue(":code", $data["code"], PDO::PARAM_INT);
        $pst->bindValue(":employee", $data["employee"], PDO::PARAM_INT);
        $pst->bindValue(":costCenter", $data["costCenter"], PDO::PARAM_STR);
        return $pst->execute();
    }

    public function getAssistants(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                assi_employee_code 'code'
            FROM assistant
            WHERE assi_training_code = :code;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function checkMinScoreRequired(int $code): int
    {
        $pst = $this->conn->prepare(
            "SELECT COUNT(assi.assi_score) 'countNotes'
                FROM training tr
                INNER JOIN assistant assi ON tr.trai_code = assi.assi_training_code
                WHERE tr.trai_code = :code AND assi.assi_score > -1 LIMIT 1;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        $countNotes = $pst->fetchColumn();
        if ($countNotes) {
            return $countNotes;
        }else {
            return 0;
        }
    }

    public function countAssistants(int $code): int
    {
        $pst = $this->conn->prepare(
            "SELECT COUNT(assi.assi_employee_code)
                FROM training tr
                INNER JOIN assistant assi ON tr.trai_code = assi.assi_training_code
                WHERE tr.trai_code = :code LIMIT 1;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        $countAssistants = $pst->fetchColumn();
        if ($countAssistants) {
            return $countAssistants;
        }else {
            return 0;
        }
    }

    public function getAssistance(int $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                assi_employee_code 'code', assi_cost_center 'costCenter', assi_assistant 'assistant', assi_score 'score'
            FROM assistant
            WHERE assi_training_code = :code;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function updateAssistance(array $data): bool
    {
        $pst = $this->conn->prepare(
            "UPDATE assistant
                SET assi_assistant = :assistant, assi_score = :score
                WHERE assi_training_code = :training AND assi_employee_code = :employee;"
        );
        $pst->bindValue(":assistant", $data["assistant"], PDO::PARAM_INT);
        $pst->bindValue(":score", $data["score"], PDO::PARAM_STR);
        $pst->bindValue(":training", $data["training"], PDO::PARAM_INT);
        $pst->bindValue(":employee", $data["employee"], PDO::PARAM_STR);
        return $pst->execute();
    }

    public function updateAssistanceToFalse(int $code): bool
    {
        $pst = $this->conn->prepare(
            "UPDATE assistant
                SET assi_assistant = 0, assi_score = NULL
                WHERE assi_training_code = :code;"
        );
        $pst->bindValue(":code", $code, PDO::PARAM_INT);
        return $pst->execute();
    }
}
