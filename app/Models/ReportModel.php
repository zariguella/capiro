<?php

class ReportModel extends Model
{
    private $tblName, $fields;

    public function getByProductionCenter(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT trai_code 'code', trai_state 'state', trai_date_week 'week', trai_date_format 'date', trai_process 'process', trai_theme 'theme'
                FROM training
                WHERE trai_date_year = :year AND trai_production_center = :productionCenter;"
        );
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByEffectiveness(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT MONTH(tr.trai_date_format) month ,
                    AVG (assi.assi_score) percent
                FROM training tr
                INNER JOIN assistant assi ON
                tr.trai_code = assi.assi_training_code

                WHERE assi.assi_score > 0 AND tr.trai_theme = :theme AND tr.trai_production_center = :productionCenter AND YEAR(tr.trai_date_format) = :year
                GROUP BY MONTH(tr.trai_date_format),YEAR(tr.trai_date_format)"
        );
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByEmployee(string $code): array
    {
        $pst = $this->conn->prepare(
            "SELECT training.trai_code 'code', training.trai_date_format 'date', assistant.assi_cost_center 'costCenter', assistant.assi_score 'score', theme.them_name 'theme', process.proc_name 'process', productioncenter.prodcent_name 'productionCenter'
                FROM training
                INNER JOIN assistant ON assistant.assi_training_code = training.trai_code
                INNER JOIN theme ON theme.them_code = training.trai_theme
                INNER JOIN process ON process.proc_code = training.trai_process
                INNER JOIN productioncenter ON productioncenter.prodcent_code = training.trai_production_center
                WHERE assistant.assi_employee_code = :code AND training.trai_state = 3
                ORDER BY training.trai_date_format DESC "
        );
        $pst->bindParam(":code", $code, PDO::PARAM_STR);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTrainingsByYear(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT training.trai_code 'code', training.trai_date_format 'date', training.trai_theme 'theme', theme.them_name 'themeName', training.trai_process 'process', process.proc_name 'processName',
                    training.trai_addressed 'addressed', addressed.addr_name 'addressedName', training.trai_target_people 'targetPeople', training.trai_planned_hours 'plannedHours', training.trai_az_cd 'az'
                FROM training
                INNER JOIN theme ON theme.them_code = training.trai_theme
                INNER JOIN process ON process.proc_code = training.trai_process
                INNER JOIN addressed ON addressed.addr_code = training.trai_addressed
                WHERE training.trai_production_center = :productionCenter AND training.trai_date_year = :year AND training.trai_state = 3
                ORDER BY training.trai_date_format DESC;"
        );
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTrainingsByEffectiveness(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT ass.assi_training_code as 'training', YEAR( tr. trai_date_format) as 'year', MONTH ( tr. trai_date_format) as 'month',AVG( ass.assi_score) as 'percentaje' FROM assistant as ass inner JOIN training as tr ON ass.assi_training_code = tr.trai_code WHERE ass.assi_score is not null AND tr.trai_production_center = :productionCenter AND tr.trai_date_year = :year AND tr.trai_state = 3 GROUP BY ass.assi_training_code, YEAR( tr. trai_date_format) , MONTH ( tr. trai_date_format) HAVING AVG( ass.assi_score) >= 80
                ORDER BY tr.trai_date_format DESC;"
        );
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByProductionCenterProgrammedCount(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    COUNT(IF( month(tr.trai_date_format) = 01, tr.trai_code, NULL)) '0',
                    COUNT(IF( month(tr.trai_date_format) = 02, tr.trai_code, NULL)) '1',
                    COUNT(IF( month(tr.trai_date_format) = 03, tr.trai_code, NULL)) '2',
                    COUNT(IF( month(tr.trai_date_format) = 04, tr.trai_code, NULL)) '3',
                    COUNT(IF( month(tr.trai_date_format) = 05, tr.trai_code, NULL)) '4',
                    COUNT(IF( month(tr.trai_date_format) = 06, tr.trai_code, NULL)) '5',
                    COUNT(IF( month(tr.trai_date_format) = 07, tr.trai_code, NULL)) '6',
                    COUNT(IF( month(tr.trai_date_format) = 08, tr.trai_code, NULL)) '7',
                    COUNT(IF( month(tr.trai_date_format) = 09, tr.trai_code, NULL)) '8',
                    COUNT(IF( month(tr.trai_date_format) = 10, tr.trai_code, NULL)) '9',
                    COUNT(IF( month(tr.trai_date_format) = 11, tr.trai_code, NULL)) '10',
                    COUNT(IF( month(tr.trai_date_format) = 12, tr.trai_code, NULL)) '11'
                    FROM training tr
                    WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 3) AND tr.trai_date_year = :year AND tr.trai_theme = :theme AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByProductionCenterExecutedCount(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    COUNT(IF( month(tr.trai_date_format) = 01, tr.trai_code, NULL)) '0',
                    COUNT(IF( month(tr.trai_date_format) = 02, tr.trai_code, NULL)) '1',
                    COUNT(IF( month(tr.trai_date_format) = 03, tr.trai_code, NULL)) '2',
                    COUNT(IF( month(tr.trai_date_format) = 04, tr.trai_code, NULL)) '3',
                    COUNT(IF( month(tr.trai_date_format) = 05, tr.trai_code, NULL)) '4',
                    COUNT(IF( month(tr.trai_date_format) = 06, tr.trai_code, NULL)) '5',
                    COUNT(IF( month(tr.trai_date_format) = 07, tr.trai_code, NULL)) '6',
                    COUNT(IF( month(tr.trai_date_format) = 08, tr.trai_code, NULL)) '7',
                    COUNT(IF( month(tr.trai_date_format) = 09, tr.trai_code, NULL)) '8',
                    COUNT(IF( month(tr.trai_date_format) = 10, tr.trai_code, NULL)) '9',
                    COUNT(IF( month(tr.trai_date_format) = 11, tr.trai_code, NULL)) '10',
                    COUNT(IF( month(tr.trai_date_format) = 12, tr.trai_code, NULL)) '11'
                    FROM training tr
                    WHERE tr.trai_state= 3 AND tr.trai_date_year = :year AND tr.trai_theme = :theme AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByProductionCenterProgrammedHours(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    SUM(IF( month(tr.trai_date_format) = 01,tr.trai_planned_hours, 0)) '0',
                    SUM(IF( month(tr.trai_date_format) = 02,tr.trai_planned_hours, 0)) '1',
                    SUM(IF( month(tr.trai_date_format) = 03,tr.trai_planned_hours, 0)) '2',
                    SUM(IF( month(tr.trai_date_format) = 04,tr.trai_planned_hours, 0)) '3',
                    SUM(IF( month(tr.trai_date_format) = 05,tr.trai_planned_hours, 0)) '4',
                    SUM(IF( month(tr.trai_date_format) = 06,tr.trai_planned_hours, 0)) '5',
                    SUM(IF( month(tr.trai_date_format) = 07,tr.trai_planned_hours, 0)) '6',
                    SUM(IF( month(tr.trai_date_format) = 08,tr.trai_planned_hours, 0)) '7',
                    SUM(IF( month(tr.trai_date_format) = 09,tr.trai_planned_hours, 0)) '8',
                    SUM(IF( month(tr.trai_date_format) = 10,tr.trai_planned_hours, 0)) '9',
                    SUM(IF( month(tr.trai_date_format) = 11,tr.trai_planned_hours, 0)) '10',
                    SUM(IF( month(tr.trai_date_format) = 12,tr.trai_planned_hours, 0)) '11'
                FROM training tr
                WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 3)
                    AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter AND tr.trai_theme = :theme;"
        );
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getByProductionCenterExecutedHours(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 01, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '0',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 02, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '1',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 03, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '2',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 04, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '3',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 05, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '4',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 06, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '5',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 07, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '6',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 08, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '7',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 09, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '8',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 10, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '9',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 11, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '10',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 12, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '11'
                FROM training tr
                WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 3)
                    AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter AND tr.trai_theme = :theme;"
        );
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTotalsProgrammedCount(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    COUNT(IF( month(tr.trai_date_format) = 01, tr.trai_code, NULL)) '0',
                    COUNT(IF( month(tr.trai_date_format) = 02, tr.trai_code, NULL)) '1',
                    COUNT(IF( month(tr.trai_date_format) = 03, tr.trai_code, NULL)) '2',
                    COUNT(IF( month(tr.trai_date_format) = 04, tr.trai_code, NULL)) '3',
                    COUNT(IF( month(tr.trai_date_format) = 05, tr.trai_code, NULL)) '4',
                    COUNT(IF( month(tr.trai_date_format) = 06, tr.trai_code, NULL)) '5',
                    COUNT(IF( month(tr.trai_date_format) = 07, tr.trai_code, NULL)) '6',
                    COUNT(IF( month(tr.trai_date_format) = 08, tr.trai_code, NULL)) '7',
                    COUNT(IF( month(tr.trai_date_format) = 09, tr.trai_code, NULL)) '8',
                    COUNT(IF( month(tr.trai_date_format) = 10, tr.trai_code, NULL)) '9',
                    COUNT(IF( month(tr.trai_date_format) = 11, tr.trai_code, NULL)) '10',
                    COUNT(IF( month(tr.trai_date_format) = 12, tr.trai_code, NULL)) '11'
                    FROM training tr
                    WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 3) AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTotalsExecutedCount(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    COUNT(IF( month(tr.trai_date_format) = 01, tr.trai_code, NULL)) '0',
                    COUNT(IF( month(tr.trai_date_format) = 02, tr.trai_code, NULL)) '1',
                    COUNT(IF( month(tr.trai_date_format) = 03, tr.trai_code, NULL)) '2',
                    COUNT(IF( month(tr.trai_date_format) = 04, tr.trai_code, NULL)) '3',
                    COUNT(IF( month(tr.trai_date_format) = 05, tr.trai_code, NULL)) '4',
                    COUNT(IF( month(tr.trai_date_format) = 06, tr.trai_code, NULL)) '5',
                    COUNT(IF( month(tr.trai_date_format) = 07, tr.trai_code, NULL)) '6',
                    COUNT(IF( month(tr.trai_date_format) = 08, tr.trai_code, NULL)) '7',
                    COUNT(IF( month(tr.trai_date_format) = 09, tr.trai_code, NULL)) '8',
                    COUNT(IF( month(tr.trai_date_format) = 10, tr.trai_code, NULL)) '9',
                    COUNT(IF( month(tr.trai_date_format) = 11, tr.trai_code, NULL)) '10',
                    COUNT(IF( month(tr.trai_date_format) = 12, tr.trai_code, NULL)) '11'
                    FROM training tr
                    WHERE tr.trai_state= 3 AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTotalsProgrammedHours(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    SUM(IF( month(tr.trai_date_format) = 01,tr.trai_planned_hours, 0)) '0',
                    SUM(IF( month(tr.trai_date_format) = 02,tr.trai_planned_hours, 0)) '1',
                    SUM(IF( month(tr.trai_date_format) = 03,tr.trai_planned_hours, 0)) '2',
                    SUM(IF( month(tr.trai_date_format) = 04,tr.trai_planned_hours, 0)) '3',
                    SUM(IF( month(tr.trai_date_format) = 05,tr.trai_planned_hours, 0)) '4',
                    SUM(IF( month(tr.trai_date_format) = 06,tr.trai_planned_hours, 0)) '5',
                    SUM(IF( month(tr.trai_date_format) = 07,tr.trai_planned_hours, 0)) '6',
                    SUM(IF( month(tr.trai_date_format) = 08,tr.trai_planned_hours, 0)) '7',
                    SUM(IF( month(tr.trai_date_format) = 09,tr.trai_planned_hours, 0)) '8',
                    SUM(IF( month(tr.trai_date_format) = 10,tr.trai_planned_hours, 0)) '9',
                    SUM(IF( month(tr.trai_date_format) = 11,tr.trai_planned_hours, 0)) '10',
                    SUM(IF( month(tr.trai_date_format) = 12,tr.trai_planned_hours, 0)) '11'
                FROM training tr
                WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 3) AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getTotalsExecutedHours(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 01, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '0',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 02, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '1',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 03, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '2',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 04, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '3',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 05, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '4',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 06, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '5',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 07, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '6',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 08, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '7',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 09, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '8',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 10, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '9',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 11, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '10',
                    SEC_TO_TIME(SUM(IF( month(tr.trai_date_format) = 12, TIME_TO_SEC(tr.trai_end_time) - TIME_TO_SEC(tr.trai_start_time), 0))) '11'
                FROM training tr
                WHERE tr.trai_state= 3 AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getCoverageTarget(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    SUM(IF( month(tr.trai_date_format) = 01, tr.trai_target_people, 0)) '0',
                    SUM(IF( month(tr.trai_date_format) = 02, tr.trai_target_people, 0)) '1',
                    SUM(IF( month(tr.trai_date_format) = 03, tr.trai_target_people, 0)) '2',
                    SUM(IF( month(tr.trai_date_format) = 04, tr.trai_target_people, 0)) '3',
                    SUM(IF( month(tr.trai_date_format) = 05, tr.trai_target_people, 0)) '4',
                    SUM(IF( month(tr.trai_date_format) = 06, tr.trai_target_people, 0)) '5',
                    SUM(IF( month(tr.trai_date_format) = 07, tr.trai_target_people, 0)) '6',
                    SUM(IF( month(tr.trai_date_format) = 08, tr.trai_target_people, 0)) '7',
                    SUM(IF( month(tr.trai_date_format) = 09, tr.trai_target_people, 0)) '8',
                    SUM(IF( month(tr.trai_date_format) = 10, tr.trai_target_people, 0)) '9',
                    SUM(IF( month(tr.trai_date_format) = 11, tr.trai_target_people, 0)) '10',
                    SUM(IF( month(tr.trai_date_format) = 12, tr.trai_target_people, 0)) '11'
                FROM training tr
                WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 4  OR tr.trai_state= 3)
                        AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter AND tr.trai_theme = :theme;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }

    public function getCoverageTrained(array $data): array
    {
        $pst = $this->conn->prepare(
            "SELECT
                    COUNT(IF( month(tr.trai_date_format) = 01 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '0',
                    COUNT(IF( month(tr.trai_date_format) = 02 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '1',
                    COUNT(IF( month(tr.trai_date_format) = 03 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '2',
                    COUNT(IF( month(tr.trai_date_format) = 04 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '3',
                    COUNT(IF( month(tr.trai_date_format) = 05 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '4',
                    COUNT(IF( month(tr.trai_date_format) = 06 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '5',
                    COUNT(IF( month(tr.trai_date_format) = 07 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '6',
                    COUNT(IF( month(tr.trai_date_format) = 08 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '7',
                    COUNT(IF( month(tr.trai_date_format) = 09 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '8',
                    COUNT(IF( month(tr.trai_date_format) = 10 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '9',
                    COUNT(IF( month(tr.trai_date_format) = 11 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '10',
                    COUNT(IF( month(tr.trai_date_format) = 12 AND ass.assi_assistant = 1 , ass.assi_assistant, NULL)) '11'
                FROM training tr
                    INNER JOIN assistant ass
                    ON tr.trai_code = ass.assi_training_code
                WHERE (tr.trai_state = 1 OR tr.trai_state = 2 OR tr.trai_state= 4  OR tr.trai_state= 3)
                        AND tr.trai_date_year = :year AND tr.trai_production_center = :productionCenter AND tr.trai_theme = :theme;
        ");
        $pst->bindParam(":year", $data["year"], PDO::PARAM_INT);
        $pst->bindParam(":productionCenter", $data["productionCenter"], PDO::PARAM_INT);
        $pst->bindParam(":theme", $data["theme"], PDO::PARAM_INT);
        $pst->execute();
        return $pst->fetchAll();
    }
}
