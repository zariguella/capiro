<style media="screen">
    .my-app-logo {
        margin-top: -0.8rem;
        margin-bottom: -2rem;
    }
</style>
<nav id="overallNabvar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand router-link" href="<?php echo APP['home']; ?>" data-linkto="mainContent">
        <img src="app-public/assets/images/trainings-log-tr-2.png" width="auto" height="49" class="align-top my-app-logo" alt="">
        <?php //echo APP["title"]; ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link router-link" href="calendar" data-linkto="mainContent">Calendario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link router-link" href="training" data-linkto="mainContent">Capacitaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link router-link" href="reports" data-linkto="mainContent">Reportes</a>
            </li>
        </ul>
        <ul class="navbar-nav my-2 my-lg-0">
            <li class="nav-item dropdown">
                <div class="notication-index">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownNotifications" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" @click="clearNotifications">
                        <span data-toggle="tooltip" data-placement="left" title="Notificaciones"><i class="far fa-bell"></i>
                            <span v-if="readed" class="ml-1 badge badge-pill badge-success my-noti-badge"><i class="fas fa-check"></i></span>
                            <span v-else class="ml-1 badge badge-pill badge-warning my-noti-badge"><i class="fas fa-info"></i></span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right my-noti-menu" aria-labelledby="dropdownNotifications">
                        <div class="my-noti-title pl-2 text-center">
                            <span class="text-muted bt-2"><small>Notificaciones</small></span>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="my-noti-body">
                            <div class="my-noti-list">
                                <div class="my-noti-item" v-for="item in notifications" id="'nt-' + item.code">
                                    <small>
                                        <a class="dropdown-item router-link" :href="'training/edit-in-execution#' + item.trainingCode" data-linkto="mainContent" data-from="dropdownNotifications">
                                            <span class="text-primary font-weight-bold">{{ item.title }}</span><span class="mx-2 text-muted">{{ item.date }}</span>
                                            <span class="d-block"></span>
                                            <p class="text-muted my-normal-wrap">{{ item.message.substr(0, 75) }}...</p>
                                        </a>
                                    </small>
                                    <div class="dropdown-divider"></div>
                                </div>
                                <div class="my-noti-item text-center" v-if="notifications.length < 1">
                                    <small class="text-center">
                                        <span class="text-muted">Aquí no hay nada que ver <i class="ml-2 far fa-angry"></i></span>
                                    </small>
                                    <div class="dropdown-divider"></div>
                                </div>
                            </div>
                        </div>
                        <div class="my-noti-footer pl-2 text-center">
                            <small><a class="router-link" href="user/notifications" data-linkto="mainContent" data-from="dropdownNotifications">Ver todo</a></small>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownUserMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span data-toggle="tooltip" data-placement="left" title="Menu de usuario"><i class="fas fa-user-tie mr-2"></i> {{ subName }}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUserMenu">
                    <a class="dropdown-item router-link" href="user/account" data-linkto="mainContent" data-from="dropdownUserMenu"><i class="fas fa-user mr-3"></i>Cuenta</a>
                    <a class="dropdown-item" href="help" target="_blank"><i class="fas fa-question-circle mr-3"></i>Ayuda</a>
                    <div class="dropdown-divider notus1"></div>
                    <h6 class="dropdown-header notus1">Opciones de administrador</h6>
                    <a class="dropdown-item router-link notus1" href="user/manage" data-linkto="mainContent" data-from="dropdownUserMenu"><i class="fas fa-user-tie mr-3"></i>Módulo de gestión</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="ajax/user/sign-out"><i class="fas fa-power-off mr-3"></i>Cerrar Sesión</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<script type="text/javascript">
    var vjUser = new Vue({
        el: "#overallNabvar",
        data: {
            JSONuser: {},
            notifications: [],
            readed: false
        },
        created: function () {
            this.loadUserInfo();
            this.getNotifications();
        },
        methods: {
            loadUserInfo: function () {
                axios.get("ajax/user/get-info").then(response => {
                    console.log(response.data);
                    this.JSONuser = response.data.data;
                    MyApp.userType = this.JSONuser.type;
                    setViewForUserType();
                });
            },
            getNotifications: function () {
                axios.get("ajax/user/get-notifications").then(response => {
                    console.log(response);
                    if (response.data.status == "success") {
                        this.notifications = response.data.data;
                    } else {
                        this.notifications = [];
                    }
                    if (this.notifications.length > 0) {
                        this.readed = false;
                    } else {
                        this.readed = true;
                    }
                });
            },
            clearNotifications: function () {
                if (this.notifications.length > 0) {
                    axios.get("ajax/user/clear-notifications");
                    this.readed = true;
                }
            }
        },
        computed: {
            subName: function () {
                var subName = this.JSONuser.name + " " + this.JSONuser.lastName;
                return subName.substr(0, 15);
            }
        }
    })
</script>
