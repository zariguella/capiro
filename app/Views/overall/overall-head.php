<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
    <base href="<?php echo APP['baseUrl']; ?>">
    <link rel="icon" type="image/png" href="app-public/assets/images/icon-64x.png" />
    <meta charset="utf-8">
    <title></title>
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">-->
    <link rel="stylesheet" href="app-public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="app-public/assets/css/app.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="app-public/assets/css/pp-alert.css">
    <script src="app-public/assets/js/app.js"></script>
    <script src="app-public/assets/js/pp-alert.js"></script>
    <script src="app-public/assets/js/nav.js"></script>
    <script src="app-public/bin/js/test-validator.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>-->
    <script src="app-public/assets/js/jquery.min.js"></script>
    <script src="app-public/assets/js/popper.min.js"></script>
    <script src="app-public/assets/js/bootstrap.min.js"></script>
    <?php
        if ($allLibraries) {
    ?>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/es.js"></script>
    <?php
        }
    ?>
    <script type="text/javascript">
        urlSub = 9;
        MyApp.currentPage = location.pathname.substr(urlSub);
        MyApp.title = "<?php echo APP["title"]; ?>";
    </script>
    <script type="text/javascript">
        axios.interceptors.request.use(function(config) {
            ppAlert("icon", "loading");
            return config;
        }, function(error) {
            ppAlert("icon", "noConn");
            return Promise.reject(error);
        });

        axios.interceptors.response.use(function(response) {
            if (response.data.msg == "no-msg") {
                ppAlert("icon", response.data.status);
            } else if (response.data.status == "redir") {
                window.location = response.data.data.url;
            } else if (response.data.status == "success") {
                ppAlert("alert", "success", {
                    msg: response.data.msg
                });
            } else if (response.data.status == "error") {
                ppAlert("alert", "error", {
                    msg: response.data.msg
                });
            } else if (response.data.status == "info") {
                ppAlert("alert", "info", {
                    msg: response.data.msg
                });
            } else if (response.data.status == "warning") {
                ppAlert("alert", "warning", {
                    msg: response.data.msg
                });
            } else {
                ppAlert("icon", "success");
            }
            console.log(response.data);
            return response;
        }, function(error) {
            ppAlert("icon", "noConn");
            return Promise.reject(error);
        });

        var previousRef = "";
        $(document).on("click", ".router-link", function() {
            var container = $(this).data("linkto");
            var href = $(this).attr('href');
            if (MyApp.currentPage == href) {
                return false;
            } else {
                if (previousRef !== "") {
                    $(previousRef).removeClass("active");
                }
                $(this).addClass("active");
                if ($(this).data("from") !== undefined){
                    $("#" + $(this).data("from")).dropdown('toggle');
                }
                history.pushState(null, "", href);
                MyApp.currentPage = href;
                axios.get("component/" + href).then(response => {
                    $("#" + container).html(response.data);
                    buildOverallNav(MyApp.currentPage);
                });
                previousRef = this;
            }
            return false;
        });
    </script>
</head>
<body class="bg-light">
