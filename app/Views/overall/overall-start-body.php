<div class="container mt-3 bg-light rounded pb-4">
    <?php if ($navigationBar){ ?>
        <div id="navigationBar" class="mb-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div id="navContainer" class="m-2"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<div id="mainContent">
