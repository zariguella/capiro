<?php
/**
 * Controlador de la entidad CAPACITACIÓN
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class TrainingController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("Training");
    }

    /**
     * Crea una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function new(array $data = null): array
    {
        //Zona de validación
        $gump = new GUMP();
        $gump->validation_rules(array(
            "week" => "required|integer|min_len,1|max_len,2",
            "year" => "required|integer|min_len,4|max_len,4",
            "date" => "required",
            "targetPeople" => "required|integer|min_len,1|max_len,6",
            "plannedHours" => "required|numeric|min_len,1|max_len,6",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "process" => "required|integer|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6",
            "addressed" => "required|integer|min_len,1|max_len,6",
            "responsable" => "required|integer|min_len,1|max_len,6",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        if ($data["targetPeople"] < 1) {
            return endArray("error", "<b>Pesonas objetivo</b> no puede ser menor a 1.");
        }
        if ($data["plannedHours"] < 0.25 && $data["plannedHours"] < 13) {
            return endArray("error", "<b>Horas planeadas</b> no puede ser menor a 0.25 horas.");
        }
        //Fin zona de validación
        $data["plannedHours"] = number_format($data["plannedHours"], 2);
        $trainingModel = new TrainingModel();
        $data["code"] = $trainingModel->getLastCode() + 1;

        if ($trainingModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Modifica una capacitación con estado Planeada
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function editPlanned(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "state" => "required|integer|min_len,1|max_len,2",
            "week" => "required|integer|min_len,1|max_len,2",
            "targetPeople" => "required|integer|min_len,1|max_len,6",
            "plannedHours" => "required|numeric|min_len,1|max_len,6",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "process" => "required|integer|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6",
            "addressed" => "required|integer|min_len,1|max_len,6",
            "responsable" => "required|integer|min_len,1|max_len,6",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        if ($data["targetPeople"] < 1) {
            return endArray("error", "<b>Pesonas objetivo</b> no puede ser menor a 1.");
        }
        if ($data["plannedHours"] < 0.25) {
            return endArray("error", "<b>Horas planeadas</b> no puede ser menor a 0.25 horas.");
        }

        $code = $data["code"];
        unset($data["code"]);

        $trainingModel = new TrainingModel();
        if ($trainingModel->updatePlanned($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Modifica el estado de una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function editState(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "state" => "integer|min_len,1|max_len,2"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($trainingModel->updateState($data["code"], $data["state"])) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Modifica una capacitación con estado En Ejecución
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function editInExecution(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "week" => "required|integer|min_len,1|max_len,2",
            "date" => "required|date",
            "targetPeople" => "required|integer|min_len,1|max_len,6",
            "plannedHours" => "required|numeric|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6",
            "addressed" => "required|integer|min_len,1|max_len,6",
            "responsable" => "required|integer|min_len,1|max_len,6",
            "place" => "required|integer|min_len,1|max_len,6",
            "percentEvaluate" => "required|integer|min_len,1|max_len,3",
            "startTime" => "required|min_len,5",
            "responsableName" => "required|alpha_space|max_len,50",
            "trainerName" => "required|alpha_space|max_len,50",
            "trainerPosition" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        if (!DateTime::createFromFormat('H:i', $data["startTime"])) {
            return endArray("error", "La <b>hora inicio</b> tiene un formato incorrecto.");
        }
        if ($data["targetPeople"] < 1) {
            return endArray("error", "<b>Pesonas objetivo</b> no puede ser menor a 1.");
        }
        if ($data["plannedHours"] < 0.1) {
            return endArray("error", "<b>Horas planeadas</b> no puede ser 0.");
        }
        if ($data["percentEvaluate"] < 20 || $data["percentEvaluate"] > 100) {
            return endArray("error", "<b>Personas a evaluar</b> no puede ser menor a 20 o mayor a 100.");
        }

        $code = $data["code"];
        unset($data["code"]);
        unset($data["productionCenter"]);
        unset($data["process"]);

        $trainingModel = new TrainingModel();
        //Generador de AZ
        if (!$trainingModel->getAZByCode($code)) {
            if (!($data["az"] = $this->AZGenerator($code))) {
                return endArray("error", "No se pudo generar el codigo <b>A-Z</b>.");
            }
        }

        if ($trainingModel->updateInExecution($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Modifica una capacitación con estado En Ejecución
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function editExecuted(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "startTime" => "required",
            "endTime" => "required"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        if (!DateTime::createFromFormat('H:i', $data["startTime"]) || !DateTime::createFromFormat('H:i', $data["endTime"])) {
            return endArray("error", "La <b>hora inicio</b> u <b>hora fin</b> tienen un formato incorrecto.");
        }
        if (DateTime::createFromFormat('H:i', $data["startTime"]) >= DateTime::createFromFormat('H:i', $data["endTime"])) {
            return endArray("error", "<b>Hora fin</b> no puede ser menor o igual a <b>hora inicio</b>.");
        }

        $code = $data["code"];
        unset($data["code"]);

        $trainingModel = new TrainingModel();
        if ($trainingModel->updateExecuted($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Modifica una capacitación con estado En Ejecución y cambia el estado a Ejecutada
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function endUp(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "startTime" => "required",
            "endTime" => "required",
            "az" => "required"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        if (!DateTime::createFromFormat('H:i', $data["startTime"]) || !DateTime::createFromFormat('H:i', $data["endTime"])) {
            return endArray("error", "La <b>hora inicio</b> u <b>hora fin</b> tienen un formato incorrecto.");
        }

        $trainingModel = new TrainingModel();

        $training = $trainingModel->getExecuted($data["code"])[0];
        $countNotes = $trainingModel->checkMinScoreRequired($data["code"]);
        $countAssistants = $trainingModel->countAssistants($data["code"]);

        $numPeopleRequired = ($countAssistants * $training["percentEvaluate"]) / 100;

        if ($countNotes < $numPeopleRequired) {
            return endArray("error", "Se debe ingresar la calificación de minimo <b>{$numPeopleRequired}</b> personas, <b> {$countNotes} </b> calificacion(es) ingresada(s).");
        }

        $code = $data["code"];
        unset($data["code"]);
        $data["state"] = 3;

        if ($trainingModel->endUp($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Establece los asistentes de una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function setAssistants(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        $success = true;
        $countErrors = 0;

        //Se borran todos los asistentes realacionados a una capacitación
        $trainingModel->deleteAssistants($data["code"]);

        //Se valida si se han seleccionado asistentes
        if (!isset($data["assistants"]) || count($data["assistants"]) < 1) {
            return endArray("info", "No se ha seleccionado ningún asistente.");
        }

        new LoadController("Employee");
        $employeeController = new EmployeeController();
        //Se recorre la nueva lista de asistentes proveniente de la vista
        foreach ($data["assistants"] as $value) {

            //Se valida la existencia del empleado en nomina
            $info = $employeeController->findByCode(["code" => $value]);

            //Se valida el resultado de la busqueda
            if ($info["status"] === "success") {
                $assistant = [
                    "code" => (string) $data["code"],
                    "employee" => $value,
                    "costCenter" => $info["data"]["costCenter"]
                ];
            } else {
                $assistant = [
                    "code" => (string) $data["code"],
                    "employee" => $value,
                    "costCenter" => "(Deshabilitado)"
                ];
            }

            //Se inserta el asistente
            if (!$trainingModel->createAssistants($assistant)) {
                $success = false;
                $countErrors++;
            }
        }

        if ($success) {
            return endArray("success", "Se ha guardado correctamente.");
        }else {
            return endArray("warning", "No se pudieron instertar {$countErrors} asistente(s).");
        }
    }

    /**
     * Establece el material de una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function setTrainingMaterial(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        $success = true;
        $msg = "";

        $trainingModel->deleteTrainingMaterial($data["code"]);

        //Se valida si se han seleccionado almenos 1 material didactico
        if (!isset($data["teachingMaterials"]) || count($data["teachingMaterials"]) < 1) {
            return endArray("warning", "No se ha seleccionado ningún material.");
        }

        foreach ($data["teachingMaterials"] as $value) {
            $isValid = GUMP::is_valid($value, array(
            	"amount" => "required|integer|min_len,1|max_len,6"
            ));
            if (!($isValid === true)) {
                $value["amount"] = 1;
            }

            $tempTrainingMaterials = [
                "code" => $data["code"],
                "material" => $value["code"],
                "amount" => $value["amount"]
            ];

            //Se valida la existencia de una material en la base de datos
            if ($this->getTrainingMaterialByCode(["code" => $data["code"], "material" => $value["code"]])) {
                //Si existe se actualiza, si no se crea
                if (!$trainingModel->updateTrainingMaterial($tempTrainingMaterials)) {
                    $success = false;
                    $msg += "No se pudo actualizar un material, ";
                }
            } else {
                if (!$trainingModel->createTrainingMaterial($tempTrainingMaterials)) {
                    $success = false;
                    $msg += "No se pudo insertar un material, ";
                }
            }
        }

        if ($success) {
            return endArray("success", "Se ha guardado correctamente.");
        }else {
            return endArray("warning", $msg);
        }
    }

    /**
     * Establece la asistencia de los empleados a una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function setAssistance(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        $success = true;
        $msg = "";

        //Se actualiza la asistencia de todos los asistentes a falso
        $trainingModel->updateAssistanceToFalse($data["code"]);

        //Se valida si existen asistentes seleccionados
        if (!isset($data["assistantsAndNotes"]) || count($data["assistantsAndNotes"]) < 1) {
            return endArray("warning", "No se han seleccionado asistentes.");
        }

        foreach ($data["assistantsAndNotes"] as $value) {

            if (isset($value["score"]) && is_numeric($value["score"])) {
                if ($value["score"] >= 0 && $value["score"] <= 100) {
                    $score = $value["score"];
                } else {
                    $score = NULL;
                }
            } else {
                $score = NULL;
            }

            $tempAssistance = [
                "training" => $data["code"],
                "employee" => $value["code"],
                "assistant" => 1,
                "score" => $score
            ];

            if (!$trainingModel->updateAssistance($tempAssistance)) {
                $success = false;
                $msg += "El asistente con codigo: <b>{$value["code"]}</b> no ha sido agregado.<br>";
            }
        }
        if ($success) {
            return endArray("success", "Se ha guardado correctamente.");
        }else {
            return endArray("warning", $msg);
        }
    }

    /**
     * Elimina una capacitación (No esta en uso)
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function delete(array $data = null): array
    {
        return endArray("error", "Algo totalmente inesperado ha ocurrido.");

        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($trainingModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Obtiene las capacitaciones con estado En Ejecución
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getListInExecution(): array
    {
        $trainingModel = new TrainingModel();
        if ($trainings = $trainingModel->getListInExecution()) {
            return endArray("success", "no-msg", $trainings);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    /**
     * Obtiene las capacitaciones con estado Planeada
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getPlanned(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($training = $trainingModel->getPlanned($data["code"])) {
            return endArray("success", "no-msg", $training[0]);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    /**
     * Obtiene las capacitaciones con estado En Ejecución
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getInExecution(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($training = $trainingModel->getInExecution($data["code"])) {
            return endArray("success", "no-msg", $training[0]);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    /**
     * Obtiene las capacitaciones con estado Ejecutada
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getExecuted(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($training = $trainingModel->getExecuted($data["code"])) {
            return endArray("success", "no-msg", $training);
        }else {
            return endArray("info", "La capacitación buscada no existe o tiene un estado diferente a planeda.");
        }
    }

    /**
     * Obtiene todas la capacitaciones
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getAll(): array
    {
        $themeModel = new ThemeModel();
        if ($themes = $themeModel->getAll()) {
            return endArray("success", "no-msg", $themes);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    /**
     * Obtiene una capacitación por copdigo
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $themeModel = new ThemeModel();
        if ($themes = $themeModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $themes);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    /**
     * Obtiene las capacitaciones por ao
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getByYear(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($trainings = $trainingModel->getByYear($data)) {
            return endArray("success", "no-msg", $trainings);
        }else {
            return endArray("info", "no-msg");
        }
    }

    /**
     * Obtiene una capacitación con toda su información
     * @return array [Información de estatus y capacitación encontradas]
     */
    public function getToView(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($training = $trainingModel->getToView($data["code"])) {
            $training = $training[0];
            //Se obtienen los materiales seleccionados para una capacitación
            $training["materials"] = $this->getMaterialsSelected(
                ["code" => $data["code"]
            ])["data"]["text"];

            return endArray("success", "no-msg", $training);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    /**
     * Se obtienen los materiales seleccionados de una capacitación
     * @param  array $data [Información obtenida del controlador]
     * @return array        [Información de estatus y materiales encontrados]
     */
    public function getMaterialsSelected(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($materials = $trainingModel->getMaterialsSelected($data["code"])) {

            $strMaterials = "";
            $count = 0;
            //Se recorren los materiales encontrados y se le da un formato de 1 sola cadena de texto
            foreach ($materials as $value) {
                if ($count < 1) {
                    $strMaterials .= $value["materialName"] . " (" . $value["amount"] . ")";
                } else {
                    $strMaterials .= ", " . $value["materialName"] . " (" . $value["amount"] . ")";
                }
                $count++;
            }

            return endArray("success", "no-msg", ["text" => $strMaterials]);
        }else {
            return endArray("info", "No hay registros.", ["text" => "Ningún material seleccionado."]);
        }
    }

    /**
     * Genera el codigo az para una capacitación
     * @param  int    $code [Codigo perteneciente a una capacitación]
     * @return string       [Codigo az generadó]
     */
    protected function AZGenerator(int $code): string
    {
        $trainingModel = new TrainingModel();

        if ($incompletedAZ = $trainingModel->getIncompletedAZ($code)) {
            if ($lastAZ = $trainingModel->getLastAZ($incompletedAZ)) {
                $lastNum = explode("-", $lastAZ)[2];
                return $incompletedAZ . '-' . (string) ((int) $lastNum + 1);
            } else {
                return $incompletedAZ . '-1';
            }
        } else {
            return false;
        }
    }

    /**
     * Se obtienen los materiales de una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus y materiales encontrados]
     */
    public function getTeachingMaterial(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        new LoadController("TeachingMaterial");
        $materials = (new TeachingMaterialController)->get()["data"];

        $trainingModel = new TrainingModel();
        if ($trainingMaterials = $trainingModel->getTeachingMaterial($data["code"])) {

            foreach ($trainingMaterials as $valueI) {
                foreach ($materials as &$valueJ) {
                    if ($valueI["code"] == $valueJ["code"]) {
                        $valueJ = array_merge($valueJ, $valueI);
                        $valueJ["selected"] = (bool) $valueJ["selected"];
                    }
                }
            }
            return endArray("success", "no-msg", $materials);
        }else {
            return endArray("warning", "No hay seleccionados.", $materials);
        }
    }

    /**
     * Se Obtiene la asitencia de los empleados a una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus y asistentes encontrados]
     */
    public function getAssistance(array $data): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($assistants = $trainingModel->getAssistance($data["code"])) {

            new LoadController("Employee");
            $employeeController = new EmployeeController();

            foreach ($assistants as &$value) {

                $info = $employeeController->findByCode(["code" => $value["code"]]);

                if ($info["status"] === "success") {
                    $value = array_merge($value, $info["data"]);
                } else {
                    $value = array_merge($value, [
                        "name" => "(Deshabilitado)",
                        "lastName" => "(Deshabilitado)"
                    ]);
                }

                $value["code"] = (string) $value["code"];
                $value["assistant"] = (bool) $value["assistant"];
            }
            return endArray("success", "no-msg", $assistants);
        }else {
            return endArray("warning", "No hay asistentes en la capacitación seleccionada.");
        }
    }

    /**
     * Se obtienen las personas seleccionadas para asistir a una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Información de estatus]
     */
    public function getAssistants(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainingModel = new TrainingModel();
        if ($assistants = $trainingModel->getAssistants($data["code"])) {
            foreach ($assistants as &$value) {
                $value = (string) $value["code"];
            }
            return endArray("success", "no-msg", $assistants);
        }else {
            return endArray("warning", "No hay seleccionados.");
        }
    }

    /**
     * Se obtiene la existencia de un material didactico en una capacitación
     * @param  array $data [Proviene del controlador, información enviada por metodo POST]
     * @return array        [Estado del material]
     */
    public function getTrainingMaterialByCode(array $data = null): bool
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|integer|min_len,1|max_len,11",
            "material" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return false;
        }

        $trainingModel = new TrainingModel();
        return $trainingModel->getTrainingMaterialByCode($data);
    }

    public function getItemsPlanned(): array
    {
        new LoadModel("ProductionCenter");
        new LoadModel("Process");
        new LoadModel("Theme");
        new LoadModel("Addressed");
        new LoadModel("Responsable");

        $productionCenterModel = new ProductionCenterModel();
        $productionCenters = $productionCenterModel->get();

        $processModel = new ProcessModel();
        $process = $processModel->get();

        $themeModel = new ThemeModel();
        $themes = $themeModel->get();

        $addressedModel = new AddressedModel();
        $addresseds = $addressedModel->get();

        $responsableModel = new ResponsableModel();
        $responsables = $responsableModel->get();

        $trainingModel = new TrainingModel();
        $trainingStates = $trainingModel->getState();

        $data = [
            "productionCenters" => $productionCenters,
            "process" => $process,
            "themes" => $themes,
            "addresseds" => $addresseds,
            "responsables" => $responsables,
            "states" => $trainingStates
        ];

        return endArray("success", "no-msg", $data);
    }

    public function getItemsInExecution(): array
    {
        new LoadModel("ProductionCenter");
        new LoadModel("Process");
        new LoadModel("Theme");
        new LoadModel("Addressed");
        new LoadModel("Responsable");
        new LoadModel("Place");
        new LoadModel("TrainerPosition");

        $productionCenterModel = new ProductionCenterModel();
        $productionCenters = $productionCenterModel->get();

        $processModel = new ProcessModel();
        $process = $processModel->get();

        $themeModel = new ThemeModel();
        $themes = $themeModel->get();

        $addressedModel = new AddressedModel();
        $addresseds = $addressedModel->get();

        $responsableModel = new ResponsableModel();
        $responsables = $responsableModel->get();

        $placeModel = new PlaceModel();
        $places = $placeModel->get();

        $trainerPositionModel = new TrainerPositionModel();
        $trainerPositions = $trainerPositionModel->get();

        $data = [
            "productionCenters" => $productionCenters,
            "process" => $process,
            "themes" => $themes,
            "addresseds" => $addresseds,
            "responsables" => $responsables,
            "places" => $places,
            "trainerPositions" => $trainerPositions
        ];

        return endArray("success", "no-msg", $data);
    }
}
