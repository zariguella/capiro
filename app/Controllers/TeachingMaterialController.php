<?php
/**
 * Controlador de la entidad MATERIAL DIDACTICO
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class TeachingMaterialController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("TeachingMaterial");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|alpha_space|max_len,50",
                "description" => "max_len,400",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $teachingMaterialModel = new TeachingMaterialModel();

        if ($this->checkBy("name", $data["name"])) {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $teachingMaterialModel->getLastCode() + 1;

        if ($teachingMaterialModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|max_len,6",
                "name" => "alpha_space|max_len,50",
                "description" => "max_len,400",
                "state" => "numeric|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $teachingMaterialModel = new TeachingMaterialModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($teachingMaterialModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $teachingMaterialModel = new TeachingMaterialModel();
        if ($teachingMaterialModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $teachingMaterialModel = new TeachingMaterialModel();
        if ($teachingMaterial = $teachingMaterialModel->get()) {
            return endArray("success", "no-msg", $teachingMaterial);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $teachingMaterialModel = new TeachingMaterialModel();
        if ($teachingMaterial = $teachingMaterialModel->getAll()) {
            return endArray("success", "no-msg", $teachingMaterial);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $teachingMaterialModel = new TeachingMaterialModel();
        if ($teachingMaterial = $teachingMaterialModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $teachingMaterial);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new TeachingMaterialModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }
}
