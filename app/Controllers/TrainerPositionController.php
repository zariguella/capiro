<?php
/**
 * Controlador de la entidad CARGO CAPACITADOR
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class TrainerPositionController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("TrainerPosition");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|alpha_space|max_len,50",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainerPositionModel = new TrainerPositionModel();

        if ($this->checkBy("name", $data["name"])) {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $trainerPositionModel->getLastCode() + 1;

        if ($trainerPositionModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|min_len,1|max_len,6",
                "name" => "alpha_space|max_len,50",
                "state" => "numeric|min_len,1|max_len,1",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $trainerPositionModel = new TrainerPositionModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($trainerPositionModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainerPositionModel = new TrainerPositionModel();
        if ($trainerPositionModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $trainerPositionModel = new TrainerPositionModel();
        if ($trainerPosition = $trainerPositionModel->get()) {
            return endArray("success", "no-msg", $trainerPosition);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $trainerPositionModel = new TrainerPositionModel();
        if ($trainerPosition = $trainerPositionModel->getAll()) {
            return endArray("success", "no-msg", $trainerPosition);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $trainerPositionModel = new TrainerPositionModel();
        if ($trainerPosition = $trainerPositionModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $trainerPosition);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new TrainerPositionModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }
}
