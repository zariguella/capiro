<?php
use Spipu\Html2Pdf\Html2Pdf;

/**
 * Controlador del sistema para generar el documento pdf
 */

/**
 * Formato de repeticion
 * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
 * Funciones relacionadas a su nombre
 */
class DocumentsController extends Controller
{

    public function assistance(): void
    {
        if (isset($_GET["code"])) {
            $code = $_GET["code"];

            $validated = GUMP::is_valid(["code" => $code], array(
                "code" => "required|integer|min_len,1|max_len,11"
            ));

            if (!($validated === true)) {
                endAjax("error", "Alto ahi pisllo!");
            }

            //Se cargan lops modelos correspondientes y se obtiene la información requerida
            new LoadModel("Training");
            new LoadController("Training");
            $trainingModel = new TrainingModel();
            $trainingController = new TrainingController();
            $data = $trainingModel->getToView($code)[0];

            //Se obtienen los asistentes y el material
            $data["assistants"] = $trainingController->getAssistance(["code" => $code])["data"];
            $data["materials"] = $trainingController->getMaterialsSelected(["code" => $code])["data"]["text"];

            //Se formatea la hora de formato 24 a formato 12 horas
            $data["startTime"] = (string) date("g:i a",strtotime($data["startTime"]));
            $data["endTime"] = (string) date("g:i a",strtotime($data["endTime"]));
            //Se define el numero de personas a evaliuar
            $data["numPeople"] = number_format((count($data["assistants"]) * $data["percentEvaluate"]) / 100, 0);

            //Se formatean los diferentes componentes de la capacitación a mayuscula
            $data["productionCenter"] = mb_strtoupper($data["productionCenter"], 'UTF-8');
            $data["place"] = mb_strtoupper($data["place"], 'UTF-8');
            $data["theme"] = mb_strtoupper($data["theme"], 'UTF-8');
            $data["process"] = mb_strtoupper($data["process"], 'UTF-8');
            $data["addressed"] = mb_strtoupper($data["addressed"], 'UTF-8');
            $data["materials"] = mb_strtoupper($data["materials"], 'UTF-8');
            $data["objective"] = substr($data["objective"], 0, 500);
            $data["content"] = substr($data["content"], 0, 500);
            $data["objective"] = wordwrap($data["objective"], 75, "<br />");
            $data["content"] = wordwrap($data["content"], 56, "<br />");

            //Se ordena la lista de asistentes por nombre completo
            array_multisort(array_column($data["assistants"], "fullName"), SORT_ASC, $data["assistants"]);

            //Se incluye el archivo del formato y se crea el pdf
            $html = include "app/Documents/assistance/assistance.php";
            $html2pdf = new Html2Pdf("P", "A4", "es", "true", "UTF-8", [5, 18, 5, 10]);
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($html);
            $html2pdf->output("Formato de asistencia codigo:{$data['code']}.pdf");
        } else {
            echo "Error";
        }
    }
}
