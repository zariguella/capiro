<?php
/**
 * Controlador de la entidad TEMA (TÍTULO)
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class ThemeController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("Theme");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|max_len,200",
                "content" => "required|max_len,5000",
                "objective" => "required|max_len,5000"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $themeModel = new ThemeModel();

        if ($this->checkBy("name", $data["name"])) {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $themeModel->getLastCode() + 1;

        if ($themeModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6",
                "name" => "alpha_space|max_len,100",
                "content" => "max_len,1000",
                "objective" => "max_len,1000",
                "state" => "numeric|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $themeModel = new ThemeModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($themeModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $themeModel = new ThemeModel();
        if ($themeModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $themeModel = new ThemeModel();
        if ($themes = $themeModel->get()) {
            return endArray("success", "no-msg", $themes);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $themeModel = new ThemeModel();
        if ($themes = $themeModel->getAll()) {
            return endArray("success", "no-msg", $themes);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $themeModel = new ThemeModel();
        if ($themes = $themeModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $themes);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new ThemeModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }
}
