<?php

/******************************************
Controlador de la entidad CENTRO DE COSTOS *
*******************************************/

/**
 * Formato de repeticion
 * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
 * Funciones relacionadas a su nombre
 */
class CostCenterController extends Controller
{

    public function get(array $data): array
    {
       /* $dictionaries = PATH["dictionaries"];
        $costCenters = jsonToArray("{$dictionaries}cost-center-data.json", "file");
        return endArray("success", "no-msg", $costCenters);*/
        //Carga la clase que contiene la conexión a la base de datos de nomina
        new LoadModel("Roster");
        $rosterModel = new RosterModel();

        if ($costCenters = $rosterModel->getCostCenter($data["codigo"])) {
            return endArray("success", "no-msg", $costCenters);
        } 
    }
    public function getAdministrative(): array
    {
        new LoadModel("Roster");
        $rosterModel = new RosterModel();
        if ($administrative = $rosterModel->getAdministrative()){
         return endArray("success", "no-msg", $administrative);
     } else {
        return endArray("error", "No se han encontrado areas.");
    }

}

}

