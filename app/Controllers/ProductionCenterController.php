<?php
/**
 * Controlador de la entidad CENTRO DE PRODUCCIÓN
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class ProductionCenterController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("ProductionCenter");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|max_len,50",
                "abbreviation" => "required|alpha|max_len,5",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $productionCenterModel = new ProductionCenterModel();

        if ($this->checkBy("name", $data["name"]))  {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $productionCenterModel->getLastCode() + 1;

        if ($this->checkBy("abbreviation", $data["abbreviation"]))  {
            return endArray("error", "Ya hay un registro con la misma abreviatura.");
        }

        if ($productionCenterModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|max_len,6",
                "name" => "min_len,1|max_len,50",
                "abbreviation" => "alpha|max_len,5",
                "state" => "boolean"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $productionCenterModel = new ProductionCenterModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($productionCenterModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $productionCenterModel = new ProductionCenterModel();
        if ($productionCenterModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $productionCenterModel = new ProductionCenterModel();
        if ($productionCenter = $productionCenterModel->get()) {
            return endArray("success", "no-msg", $productionCenter);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $productionCenterModel = new ProductionCenterModel();
        if ($productionCenter = $productionCenterModel->getAll()) {
            return endArray("success", "no-msg", $productionCenter);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $productionCenterModel = new ProductionCenterModel();
        if ($productionCenter = $productionCenterModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $productionCenter);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function getByName(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|min_len,1|max_len,50"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $productionCenterModel = new ProductionCenterModel();
        if ($productionCenter = $productionCenterModel->getByName($data["name"])) {
            return endArray("success", "Correcto!", $productionCenter[0]);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new ProductionCenterModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }

    public function getFromRoster(): array
    {
        $dictionaries = PATH["dictionaries"];
        $productionCenters = jsonToArray("{$dictionaries}production-center-data.json", "file");
        return endArray("success", "no-msg", $productionCenters);

        new LoadModel("Roster");
        $rosterModel = new RosterModel();

        if ($productionCenters = $rosterModel->getProductionCenter()) {
            return endArray("success", "no-msg", $productionCenters);
        } else {
            return endArray("error", "No se han encontrado centros de producción.");
        }
    }
}
