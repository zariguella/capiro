<?php
/***********************************
Controlador de la entidad Dirigido *
***********************************/

/**
 * Formato de repeticion
 * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
 * Funciones relacionadas a su nombre
 */
class AddressedController extends Controller
{
    /**
     * Carga el archivo que contiene la clase modelo de Dirigido
     */
    public function loadModel(): void
    {
        new LoadModel("Addressed");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|alpha_space|max_len,50",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $addressedModel = new AddressedModel();

        if ($this->checkBy("name", $data["name"])) {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $addressedModel->getLastCode() + 1;

        if ($addressedModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|max_len,6",
                "name" => "alpha_space|max_len,50",
                "state" => "numeric|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $addressedModel = new AddressedModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($addressedModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $addressedModel = new AddressedModel();
        if ($addressedModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $addressedModel = new AddressedModel();
        if ($addressed = $addressedModel->get()) {
            return endArray("success", "no-msg", $addressed);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $addressedModel = new AddressedModel();
        if ($addressed = $addressedModel->getAll()) {
            return endArray("success", "no-msg", $addressed);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $addressedModel = new AddressedModel();
        if ($addressed = $addressedModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $addressed);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new AddressedModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }
}
