<?php
/**
 * Controlador de la entidad RESPONSABLE
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class ResponsableController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("Responsable");
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "name" => "required|alpha_space|max_len,50",
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $responsableModel = new ResponsableModel();

        if ($this->checkBy("name", $data["name"])) {
            return endArray("error", "Ya hay un registro con el mismo nombre.");
        }

        $data["code"] = $responsableModel->getLastCode() + 1;

        if ($responsableModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|numeric|max_len,6",
                "name" => "alpha_space|max_len,50",
                "state" => "numeric|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);

        $responsableModel = new ResponsableModel();

        if ($tempCode = $this->checkBy("name", $data["name"])) {
            if ($tempCode != $code) {
                return endArray("error", "Ya hay un registro con el mismo nombre.");
            }
        }

        if ($responsableModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $responsableModel = new ResponsableModel();
        if ($responsable = $responsableModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function get(): array
    {
        $responsableModel = new ResponsableModel();
        if ($responsable = $responsableModel->get()) {
            return endArray("success", "no-msg", $responsable);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getAll(): array
    {
        $responsableModel = new ResponsableModel();
        if ($responsable = $responsableModel->getAll()) {
            return endArray("success", "no-msg", $responsable);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $data = jsonToArray($_POST["data"]);

        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "numeric|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $responsableModel = new ResponsableModel();
        if ($responsable = $responsableModel->getByCode($data["code"])) {
            return endArray("success", "Correcto!", $responsable);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function checkBy(string $by, string $data): int
    {
        $addressedModel = new ResponsableModel();
        if ($code = $addressedModel->getBy($by, $data)) {
            return $code;
        }else {
            return false;
        }
    }
}
