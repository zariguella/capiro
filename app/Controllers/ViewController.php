<?php
/**
 * Clase de la aplicación encargada de cargar la vista
 */
class ViewController
{

    private $module, $subModule, $rule, $noSession;

    public function __construct()
    {

    }

    public function runFromUrl(): void
    {
        $this->module = formatView(APP["defaultView"]);
        $this->subModule = formatView(APP["defaultIndex"]);
        $this->rule = VIEW_MAP["defaultRule"];

        $this->checkModule();
        $this->checkRule();
        $this->checkPreviousStep();
        $this->checkSession();
        //$this->buildLayout();
    }

    private function checkModule(): void
    {
        if (isset($_GET["module"]) && !is_null($_GET["module"]) && $_GET["module"] !== "") {
            $this->module = formatView($_GET["module"]);
            if (isset($_GET["sub_module"]) && !is_null($_GET["sub_module"]) && $_GET["sub_module"] !== "") {
                $this->subModule = formatView($_GET["sub_module"]);
            }
        }
    }

    private function checkRule(): void
    {
        if (isset(VIEW_MAP[$this->module][$this->subModule])) {
            $this->rule = array_merge(
                $this->rule,
                VIEW_MAP[$this->module][$this->subModule]
            );
        }
        if (!$this->rule["enabled"]) {
            redirTo("error/404");
        }
    }

    private function checkPreviousStep(): void
    {
        if (isset($this->rule["previousStep"])) {
            redirTo("{$this->rule["previousStep"]}");
        }
    }

    private function checkSession(): void
    {
        if ($this->rule["session"] == Session::check("user") || $this->rule["validInSession"] == Session::check("user")) {
            if ($this->rule["session"]) {
                if (Session::check("user")) {
                    $user = Session::getByName("user");
                    if ($user->viewPermission($this->module, $this->subModule)) {
                        $this->buildLayout();
                    } else {
                        redirTo(APP["home"]);
                    }
                } else {
                    redirTo(APP["noSession"]);
                }
            } else {
                $this->buildLayout();
            }
        }else {
            if (APP["noSession"].APP["defaultIndex"] == $this->module.$this->subModule) {
                redirTo(APP["home"]);
            }else {
                redirTo(APP["noSession"]);
            }
        }
    }

    private function buildLayout(): void
    {
        $layout = new Layout(
            $this->module,
            $this->subModule,
            $this->rule["allLibraries"],
            $this->rule["navbar"],
            $this->rule["alert"],
            $this->rule["navigationBar"],
            $this->rule["content"]
        );
    }
}
