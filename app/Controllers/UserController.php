<?php
/**
 * Controlador de la entidad USUARIO
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class UserController extends Controller
{

    public function loadModel(): void
    {
        new LoadModel("User");
    }

    public function signIn(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "nick" => "required|max_len,40|min_len,3",
                "pass" => "required|max_len,24|min_len,4"
        ));

       /*$validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $nickWithDomain = DB["ActiveDirectory"]["domain"] . $data["nick"];

        new LoadModel("ActiveDirectory");
        $activeDirectoryModel = new ActiveDirectoryModel();

        $userValidated = $activeDirectoryModel->validateUser(
            $nickWithDomain,
            $data["pass"]
        );*/

        //Comentar esta linea cuando se active la vpn
        $userValidated = true;

        if ($userValidated) {

            $userModel = new UserModel();

            new LoadController("Employee");
            $employeeController = new EmployeeController();

            if (!$code = $userModel->getCodeByNick($data["nick"])) {
                return endArray("error", "Usuario no encontrado en la aplicación.");
            }

            //Comentar esta linea cuando se active la vpn
            $code = "1036938697";

            $employee = $employeeController->findByCode(["code" => $code]);

            if ($employee["status"] === "success") {
                if ($userInfo = $userModel->getByCode($code)[0]) {

                    $userInfo = array_merge($employee["data"], $userInfo);

                    new LoadController("ProductionCenter");
                    $productionCenterController = new ProductionCenterController();

                    $productionCenter = $productionCenterController->getByName(
                        ["name" => $userInfo["productionCenter"]]
                    )["data"];

                    $userInfo["productionCenter"] = $productionCenter["code"];

                    try {
                        $user = new User($userInfo);
                    } catch (\Exception $e) {
                        return endArray("error", "Ha ocurrido un error desconocido.");
                    }

                    Session::set("user", $user);

                    $audit  = new Audit();
                    $audit->create("SIGNIN");
                    
                    //Descomentar cuando se instalen drivers de sql server, y arreglar vpn
                   // $employeeController->checkUpdateTime();

                    return endArray("redir", "", ["url" => APP["home"]]);

                } else {
                    return endArray("error", "El usuario no esta registrado en la aplicación.");
                }
            } else {
                return endArray("error", "El usuario no existe en Nomina.");
            }
        } else {
            return endArray("error", "Usuario o contraseña incorrectos.");
        }
    }

    public function getInfo(): array
    {
        $userInfo = Session::use("user")->getInfo();

        $audit = new Audit();
        $userInfo["lastSignin"] = $audit->getLastSignin($userInfo["code"]);

        new LoadModel("ProductionCenter");
        $productionCenterModel = new ProductionCenterModel();

        $productionCenter = $productionCenterModel->getByCode(
            $userInfo["productionCenter"]
        )[0]["name"];

        $userInfo["productionCenterName"] = $productionCenter;

        return endArray("success", "no-msg", $userInfo);
    }

    public function signOut(): void
    {
        Session::destroyAll();
        redirTo(APP["noSession"]);
    }

    public function check(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|min_len,8|max_len,15",
                "nick" => "required|min_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        new LoadController("Employee");
        $employeeController = new EmployeeController();

        $employee = $employeeController->findByCode(["code" => $data["code"]]);

        if ($employee["status"] === "success") {
            $userModel = new UserModel();
            if (!$userModel->getByCodeOrNick($data)) {

                return endArray("success", "Usuario disponible.", $employee["data"]);
            }else {
                return endArray("error", "El usuario ya se encuentra registrado en la aplicación.");
            }
        } else {
            return endArray("error", "El usuario no existe en Nomina.");
        }
    }

    public function new(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|alpha_numeric|max_len,15|min_len,8",
                "nick" => "required|min_len,1|max_len,30",
                "type" => "required|numeric|max_len,2"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $userModel = new UserModel();

        if ($userModel->getByCodeOrNick($data)) {
            return endArray("error", "El usuario ya se encuentra registrado!");
        }

        new LoadController("Employee");
        $employeeController = new EmployeeController();

        $employee = $employeeController->findByCode(["code" => $data["code"]]);

        if ($employee["status"] === "success") {

            new LoadController("ProductionCenter");
            $productionCenterController = new ProductionCenterController();

            $productionCenter = $productionCenterController->getByName(
                ["name" => $employee["data"]["productionCenter"]]
            );

            if ($productionCenter["status"] === "success") {
                $data["productionCenter"] = $productionCenter["data"]["code"];
            } else {
                return endArray("error", "El centro de producción no existe.");
            }
        } else {
            return endArray("error", "El usuario no existe en Nomina.");
        }

        if ($userModel->create($data)) {
            return endArray("success", "Se ha creado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function edit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|alpha_numeric|max_len,15|min_len,8",
                "type" => "required|numeric|max_len,2",
                "state" => "required|numeric|max_len,1|min_len,1",
                "nick" => "required|max_len,50|min_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $code = $data["code"];
        unset($data["code"]);
        unset($data["productionCenter"]);

        $userModel = new UserModel();
        if ($userModel->update($code, $data)) {
            return endArray("success", "Se ha modificado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function editProductionCenter(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|alpha_numeric|max_len,15|min_len,8",
                "productionCenter" => "required|numeric|max_len,2"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $userModel = new UserModel();
        if ($userModel->updateProductionCenter($data["code"], $data["productionCenter"])) {
            return endArray("success", "Se ha guardado correctamente.");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function delete(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|alpha_numeric|min_len,8|max_len,15"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $userModel = new UserModel();
        if ($userModel->delete($data["code"])) {
            return endArray("success", "Se ha eliminado correctamente.");
        }else {
            return endArray("error", "El usuario no existe.");
        }
    }

    public function getByCode(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|max_len,15"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $userModel = new UserModel();
        if ($user = $userModel->getByCode($data["code"])) {
            return endArray("error", "El usuario ya se encuentra registrado!", $user);
        }else {
            return endArray("info", "Usuario disponible.");
        }
    }

    public function getToEdit(array $data = null): array
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "code" => "required|max_len,15"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $userModel = new UserModel();
        if ($user = $userModel->getByCode($data["code"])[0]) {

            new LoadController("Employee");
            $employeeController = new EmployeeController();

            $employee = $employeeController->findByCode(["code" => $data["code"]]);

            if ($employee["status"] === "success") {
                $user = array_merge($user, $employee["data"]);
            } else {
                $user = array_merge($user, [
                    "name" => "(Deshabilitado)",
                    "lastName" => "(Deshabilitado)",
                    "productionCenter" => "(Deshabilitado)"
                ]);
            }

            return endArray("success", "no-msg", $user);
        }else {
            return endArray("Error", "Usuario no encontrado.");
        }
    }

    public function getUsers(): array
    {
        $userModel = new UserModel();
        if ($users = $userModel->getUsers()) {

            new LoadController("Employee");
            $employeeController = new EmployeeController();

            foreach ($users as &$value) {

                $info = $employeeController->findByCode(["code" => $value["code"]]);

                if ($info["status"] === "success") {
                    $value = array_merge($value, $info["data"]);
                } else {
                    $value = array_merge($value, [
                        "name" => "(Deshabilitado)",
                        "lastName" => "(Deshabilitado)",
                        "productionCenter" => "(Deshabilitado)"
                    ]);
                }
            }
            return endArray("success", "no-msg", $users);
        }else {
            return endArray("info", "No hay usuarios registrados.");
        }
    }

    public function getProductionCenter(): array
    {
        return endArray("success", "no-msg", [
            "code" => Session::use("user")->getProductionCenter()
        ]);
    }

    public function getUserType(): array
    {
        $userModel = new UserModel();
        if ($userType = $userModel->getUserType()) {
            return endArray("success", "no-msg", $userType);
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }

    public function getNotifications(): array
    {
        $code = Session::use("user")->getCode();
        $userModel = new UserModel();
        if ($notifications = $userModel->getNotifications($code)) {

            foreach ($notifications as &$value) {
                $replace = [
                    ":theme" => $value["theme"],
                    ":trainingCode" => $value["trainingCode"],
                    ":trainingDate" => $value["trainingDate"]
                ];
                $value["message"] = strReplaceAssoc($replace, $value["message"]);
            }
            return endArray("success", "no-msg", $notifications);
        }else {
            return endArray("info", "no-msg");
        }
    }

    public function getAllNotifications(): array
    {
        $code = Session::use("user")->getCode();
        $userModel = new UserModel();
        if ($notifications = $userModel->getAllNotifications($code)) {

            foreach ($notifications as &$value) {
                $replace = [
                    ":theme" => $value["theme"],
                    ":trainingCode" => $value["trainingCode"],
                    ":trainingDate" => $value["trainingDate"]
                ];
                $value["message"] = strReplaceAssoc($replace, $value["message"]);
            }
            return endArray("success", "no-msg", $notifications);
        }else {
            return endArray("info", "no-msg");
        }
    }

    public function clearNotifications(): array
    {
        $code = Session::use("user")->getCode();
        $userModel = new UserModel();
        if ($userModel->clearNotifications($code)) {
            return endArray("success", "no-msg");
        }else {
            return endArray("error", "Algo totalmente inesperado ha ocurrido.");
        }
    }
}
