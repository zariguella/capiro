<?php
/***********************************
Controlador de la entidad Empleado *
***********************************/

/**
 * Formato de repeticion
 * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
 * Funciones relacionadas a su nombre
 */
class EmployeeController extends Controller
{
    private $employees;

    public function __construct()
    {
        parent::__construct();
        $this->employees = $this->loadAll();
    }

    //Se carga la lista de los empleados de nomina almacenados en cache
    public function loadAll(): array
    {
        $dictionaries = PATH["dictionaries"];
        return jsonToArray("{$dictionaries}employees-data.json", "file");
    }

    public function getAll(): array
    {
        return endArray("success", "no-msg", $this->employees);
    }

    //Se busca un solo empleado por codigo
    public function findByCode(array $data): array
    {   //$data["code"]="1001546389";
        $found = current(array_filter($this->employees, function($item) use ($data) {
            return isset($item["code"]) && $item["code"] == $data["code"];
        }));

        if ($found) {
            return endArray("success", "no-msg", $found);
        } else {
            return endArray("error", "No encotrado");
        }
    }

    //Actualiza el cache
    public function updateData(): bool
    {
        new LoadModel("Roster");
        $rosterModel = new RosterModel();
        $dictionaries = PATH["dictionaries"];

        if ($updatedEmployees = $rosterModel->getAll()) {

            $fileName = "{$dictionaries}employees-data.json";
            $file = fopen($fileName, "w");
            fwrite($file, arrayToJson($updatedEmployees));
            fclose($file);

            return true;
        } else {
            return false;
        }
    }

    //Valida la actualización del chace cada día
    public function checkUpdateTime(): void
    {

        $oldDateJson = jsonToArray("config/roster-update.json", "file");
        if ($oldDateJson["lastUpdate"] == "null") {

            $this->updateData();
            $fileName = "config/roster-update.json";

            $file = fopen($fileName, "w");
            fwrite($file, arrayToJson([
                "lastUpdate" => (new DateTime("now"))->format('Y-m-d H:i:s')
            ]));
            fclose($file);

        } else {
            if ($oldDate = new DateTime($oldDateJson["lastUpdate"])) {

                $newDate = new DateTime("now");
                $diff = $oldDate->diff($newDate);

                if ($diff->y >= 1  || $diff->m >= 1 || $diff->days >= 1) {

                    $this->updateData();
                    $fileName = "config/roster-update.json";

                    $file = fopen($fileName, "w");
                    fwrite($file, arrayToJson([
                        "lastUpdate" => (new DateTime("now"))->format('Y-m-d H:i:s')
                    ]));
                    fclose($file);
                }
            }
        }
    }

    //Valida la actualización de cache con un limite de 10 minutos
    public function checkMinUpdate(): array
    {

        $oldDateJson = jsonToArray("config/roster-update.json", "file");
        if ($oldDateJson["lastUpdate"] == "null") {

            if (!$this->updateData()) {
                return endArray("error", "Ha ocurrido un error al actualizar.");
            }

            $fileName = "config/roster-update.json";
            $file = fopen($fileName, "w");
            fwrite($file, arrayToJson([
                "lastUpdate" => (new DateTime("now"))->format('Y-m-d H:i:s')
            ]));
            fclose($file);

            return endArray("success", "no-msg");

        } else {
            if ($oldDate = new DateTime($oldDateJson["lastUpdate"])) {

                $newDate = new DateTime("now");
                $diff = $oldDate->diff($newDate);

                if ($diff->y >= 1  || $diff->m >= 1 || $diff->d >= 1 || $diff->h >= 1 || $diff->i > 10) {

                    if (!$this->updateData()) {
                        return endArray("error", "Ha ocurrido un error al actualizar.");
                    }

                    $fileName = "config/roster-update.json";
                    $file = fopen($fileName, "w");
                    fwrite($file, arrayToJson([
                        "lastUpdate" => (new DateTime("now"))->format('Y-m-d H:i:s')
                    ]));
                    fclose($file);

                    return endArray("success", "no-msg");

                } else {
                    return endArray("error", "Debes esperar <b> 10 minutos </b>.");
                }
            } else {
                return endArray("error", "Formatod de <b> fecha </b> incorrecto.");
            }
        }
    }
}
