<?php
/**
 * Controlador de los reportes
 */

 /**
  * Formato de repeticion
  * GIMP valida la información con sus propias reglas, documentacion encontrada en https://github.com/Wixel/GUMP
  * Funciones relacionadas a su nombre
  */
class ReportController extends Controller
{
    public function loadModel(): void
    {
        new LoadModel("Report");
    }

    //Se obtienen los items utilizados en la vista de los reportes
    public function getItems(array $data = null): array
    {
        new LoadModel("ProductionCenter");
        new LoadModel("Process");
        new LoadModel("Theme");
        new LoadModel("Addressed");

        $productionCenterModel = new ProductionCenterModel();
        $productionCenters = $productionCenterModel->get();

        $processModel = new ProcessModel();
        $process = $processModel->get();

        $themeModel = new ThemeModel();
        $themes = $themeModel->get();

        $addressedModel = new AddressedModel();
        $addresseds = $addressedModel->get();

        $data = [
            "productionCenters" => $productionCenters,
            "process" => $process,
            "themes" => $themes,
            "addresseds" => $addresseds
        ];

        return endArray("success", "no-msg", $data);
    }

    public function getByProductionCenter(array $data = null)
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6",
            "type" => "required|integer|min_len,1|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", "Hay filtros sin seleccionar.");
        }

        $report = [];

        //Se valida el tipo de reporte , 0 es igual a numero de capacitaciones. 1 es igual al numero de horas
        if ((int) $data["type"] === 0) {

            $reportModel = new ReportModel();
            $programmed = $reportModel->getByProductionCenterProgrammedCount($data)[0];
            $executed = $reportModel->getByProductionCenterExecutedCount($data)[0];

            $countTotalProgrammed = 0;
            $countTotalExecuted = 0;

            //Se simulan los 12 meses del año
            for ($i=0; $i < 12; $i++) {

                $report["programmed"][$i] = (int) $programmed[$i];
                $report["executed"][$i] = (int) $executed[$i];

                //Se saca el porcentaje entre numero de capacitaciones programadas y ejecutadas
                $percentage = $programmed[$i] > 0
                    ? number_format(($executed[$i] / $programmed[$i]) * 100, 0) . '%'
                    : 0 . '%';

                $report["percentage"][$i] = $percentage;

                $countTotalProgrammed += (int) $programmed[$i];
                $countTotalExecuted += (int) $executed[$i];
            }
            $report["programmed"][] = $countTotalProgrammed;
            $report["executed"][] = $countTotalExecuted;

            //Se saca el porcentaje total entre numero de capacitaciones programadas y ejecutadas
            $report["percentage"][] = $countTotalProgrammed > 0
                ? number_format(($countTotalExecuted / $countTotalProgrammed) * 100, 0) . '%'
                : 0 . '%';

        } else if ((int) $data["type"] === 1) {

            $reportModel = new ReportModel();
            $programmed = $reportModel->getByProductionCenterProgrammedHours($data)[0];
            $executed = $reportModel->getByProductionCenterExecutedHours($data)[0];

            $hoursTotalProgrammed = 0;
            $hoursTotalExecuted = 0;

            for ($i=0; $i < 12; $i++) {

                //Se convierte el tiempo entre hora inicio y hora fin a un decimal
                $arrTime = explode(":", $executed[$i]);
                $hoursExecuted = (float) ($arrTime[0]);

                $hoursExecuted += isset($arrTime[1])
                    ? number_format($arrTime[1] / 60, 2)
                    : 0;

                $report["programmed"][$i] = (float) $programmed[$i];
                $report["executed"][$i] = $hoursExecuted;

                //Se saca el porcentaje de hora programadas y ejecutadas
                $percentage = $programmed[$i] > 0
                    ? number_format(($hoursExecuted * 100) / $programmed[$i], 0) . '%'
                    : 0 . '%';

                $report["percentage"][$i] = $percentage;

                $hoursTotalProgrammed += (float) $programmed[$i];
                $hoursTotalExecuted += $hoursExecuted;
            }
            $report["programmed"][] = $hoursTotalProgrammed;
            $report["executed"][] = $hoursTotalExecuted;

            //Se saca el porcentaje total entre numero de horas programadas y ejecutadas
            $report["percentage"][] = $hoursTotalProgrammed > 0
                ? number_format(($hoursTotalExecuted / $hoursTotalProgrammed) * 100, 0) . '%'
                : 0 . '%';
        } else {
            return endArray("error", "Tipo incorrecto de indicador.");

        }
        return endArray("success", "no-msg", $report);
    }

    public function getTotals(array $data = null)
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "type" => "required|integer|min_len,1|max_len,1"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", "Hay filtros sin seleccionar.");
        }

        $report = [];

        //Se valida el tipo de reporte , 0 es igual a numero de capacitaciones. 1 es igual al numero de horas
        if ((int) $data["type"] === 0) {

            $reportModel = new ReportModel();
            $programmed = $reportModel->getTotalsProgrammedCount($data)[0];
            $executed = $reportModel->getTotalsExecutedCount($data)[0];

            $countTotalProgrammed = 0;
            $countTotalExecuted = 0;

            for ($i=0; $i < 12; $i++) {

                $report["programmed"][$i] = (int) $programmed[$i];
                $report["executed"][$i] = (int) $executed[$i];

                //Se calcula el porcentaje entre programadas y ejecutadas
                $percentage = $programmed[$i] > 0
                    ? number_format(($executed[$i] / $programmed[$i]) * 100, 0) . '%'
                    : 0 . '%';

                $report["percentage"][$i] = $percentage;
                $countTotalProgrammed += (int) $programmed[$i];
                $countTotalExecuted += (int) $executed[$i];
            }
            $report["programmed"][] = $countTotalProgrammed;
            $report["executed"][] = $countTotalExecuted;

            //Se calcula el porcentaje total entre programadas y ejecutadas
            $report["percentage"][] = $countTotalProgrammed > 0
                ? number_format(($countTotalExecuted / $countTotalProgrammed) * 100, 0) . '%'
                : 0 . '%';

        } else if ((int) $data["type"] === 1) {

            $reportModel = new ReportModel();
            $programmed = $reportModel->getTotalsProgrammedHours($data)[0];
            $executed = $reportModel->getTotalsExecutedHours($data)[0];

            $hoursTotalProgrammed = 0;
            $hoursTotalExecuted = 0;

            for ($i=0; $i < 12; $i++) {

                //Se formatea la hora en formato HH:mm a un float
                $arrTime = explode(":", $executed[$i]);
                $hoursExecuted = (float) ($arrTime[0]);
                $hoursExecuted += isset($arrTime[1])
                    ? number_format($arrTime[1] / 60, 2)
                    : 0;

                $report["programmed"][$i] = number_format((float) $programmed[$i], 2);
                $report["executed"][$i] = number_format($hoursExecuted, 2);

                //Se calcula el porcentaje entre horas programadas y ejecutadas
                $percentage = $programmed[$i] > 0
                    ? number_format(($hoursExecuted / $programmed[$i]) * 100, 0) . '%'
                    : 0 . '%';

                $report["percentage"][$i] = $percentage;

                $hoursTotalProgrammed += (float) $programmed[$i];
                $hoursTotalExecuted += $hoursExecuted;
            }

            $report["programmed"][] = number_format($hoursTotalProgrammed, 2);
            $report["executed"][] = $hoursTotalExecuted;

            //Se calcula el porcentaje total entre horas programadas y ejecutadas
            $report["percentage"][] = $hoursTotalProgrammed > 0
                ? number_format(($hoursTotalExecuted / $hoursTotalProgrammed) * 100, 0) . '%'
                : 0 . '%';

        }
        return endArray("success", "no-msg", $report);
    }

    public function getCoverage(array $data = null)
    {
        if (is_null($data)) {
            return endArray("error", "Falta información.");
        }
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6",
            "sex" => "min_len,1|max_len,20"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", "Hay filtros sin seleccionar.");
        }

        $reportModel = new ReportModel();
        $report = [];
        $sum = 0;
        $target = $reportModel->getCoverageTarget($data)[0];
        $trained = $reportModel->getCoverageTrained($data)[0];

        $countTotalTarget = 0;
        $countTotalTrained = 0;

        for ($i=0; $i < 12; $i++) {

            $report["target"][$i] = (int) $target[$i];
            $report["trained"][$i] = (int) $trained[$i];

            //Se calcula el porcentaje entre personas objetivo y personas capacitadas
            $percentage = $target[$i] > 0
                ? number_format(($trained[$i] / $target[$i]) * 100, 0) . '%'
                : 0 . '%';

            $report["percentage"][$i] = $percentage;

            $countTotalTarget += (int) $target[$i];
            $countTotalTrained += (int) $trained[$i];
        }

        $report["target"][] = $countTotalTarget;
        $report["trained"][] = $countTotalTrained;

        //Se calcula el porcentaje total entre horas programadas y ejecutadas
        $report["percentage"][] = $countTotalTarget > 0
            ? number_format(($countTotalTrained / $countTotalTarget) * 100, 0) . '%'
            : 0 . '%';

        return endArray("success", "no-msg", $report);
    }

    public function getByEffectiveness(array $data = null)
    {
        if (is_null($data)) {
            return endArray("error", "Falta información.");
        }
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6",
            "theme" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", "Hay filtros sin seleccionar.");
        }

        $reportModel = new ReportModel();
        $report = [];
        $sum = 0;
        $count = 0;

        if ($effectiveness = $reportModel->getByEffectiveness($data)) {
            //Se simulan los 12 meses
            for ($i=0; $i < 12; $i++) {
                //Dependiendo del numero del ciclo se buscará en la lista traida desde la base de datos
                //En busqueda de registros en el mes relacionado al ciclo
                $found = current(array_filter($effectiveness, function($item) use ($i) {
                    return isset($item["month"]) && $item["month"] == ($i + 1);
                }));
                //Si se encuentra un registro se realizará el formateo y se concatenará el simbolo de porcentaje
                if ($found) {
                    $sum += (float) number_format($found["percent"], 2);
                    $count++;
                    $report[] = number_format($found["percent"], 2) . '%';
                } else {
                    $report[] = '0%';
                }
            }
            //Se calcula el total
            $report[] = number_format($sum / $count, 2) . '%';
            return endArray("success", "no-msg", $report);
        } else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getByEmployee(array $data = null)
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "code" => "required|alpha_numeric|min_len,1|max_len,14"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $reportModel = new ReportModel();
        if ($report["list"] = $reportModel->getByEmployee($data["code"])) {

            new LoadController("Employee");
            $employeeController = new EmployeeController();

            $employee = $employeeController->findByCode(["code" => $data["code"]]);
            if ($employee["status"] === "success") {
                $report["employee"] = $employee["data"];
            } else {
                $report["employee"] = [
                    "fullName" => "Deshabilitado",
                    "costCenter" => "Deshabilitado",
                    "productionCenter" => "Deshabilitado"
                ];
            }

            return endArray("success", "no-msg", $report);
        }else {
            return endArray("info", "No hay registros.");
        }
    }

    public function getTrainingsByYear(array $data = null)
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $reportModel = new ReportModel();
        if ($report = $reportModel->getTrainingsByYear($data)) {
            return endArray("success", "no-msg", $report);
        }else {
            return endArray("success", "no-msg", []);
        }
    }

    public function getTrainingsByEffectiveness(array $data = null)
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
            "year" => "required|integer|min_len,4|max_len,4",
            "productionCenter" => "required|integer|min_len,1|max_len,6"
        ));

        $validatedData = $gump->run($data);
        if (!$validatedData) {
            return endArray("error", $gump->get_readable_errors(true));
        }

        $reportModel = new ReportModel();
        if ($report = $reportModel->getTrainingsByEffectiveness($data)) {
            return endArray("success", "no-msg", $report);
        }else {
            return endArray("success", "no-msg", []);
        }
    }
}
