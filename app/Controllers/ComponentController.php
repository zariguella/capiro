<?php
/**
 * Clase de la aplicación encargada de cargar partes pequeñas de esta
 */
class ComponentController
{

    private $module, $subModule, $rule;

    public function __construct()
    {

    }

    public function runFromUrl(): void
    {
        $this->subModule = formatView("index");
        $this->rule = VIEW_MAP["defaultRule"];
        $this->checkModule();
        $this->checkRule();
        $this->checkSession();
        //$this->buildComponent();
    }

    private function checkModule(): void
    {
        if (isset($_GET["module"]) && !is_null($_GET["module"]) && $_GET["module"] !== "") {
            $this->module = formatView($_GET["module"]);
            if (isset($_GET["sub_module"]) && !is_null($_GET["sub_module"]) && $_GET["sub_module"] !== "") {
                $this->subModule = formatView($_GET["sub_module"]);
            }
        }else {
            endComponent("error", "Modulo no definido.");
        }
    }

    private function checkRule(): void
    {
        if (isset(VIEW_MAP[$this->module][$this->subModule])) {
            $this->rule = array_merge($this->rule, VIEW_MAP[$this->module][$this->subModule]);
        }
        if (!$this->rule["enabled"]) {
            endComponent("not-found", "Componente no disponible.");
        }
    }

    private function checkPreviousStep(): void
    {
        if (isset($this->rule["previousStep"])) {
            endComponent("previus-step", "Te estas saltando algunos pasos.");
        }
    }

    private function checkSession(): void
    {
        if ($this->rule["session"] == Session::check("user") || $this->rule["validInSession"] == Session::check("user")) {
            if ($this->rule["session"]) {
                if (Session::check("user")) {
                    $user = Session::getByName("user");
                    if ($user->viewPermission($this->module, $this->subModule)) {
                        $this->buildComponent();
                    } else {
                        $this->module = "error";
                        $this->subModule = "permission-required";
                        $this->buildComponent();
                    }
                } else {
                    $this->module = "error";
                    $this->subModule = "session-required";
                    $this->buildComponent();
                }
            } else {
                $this->buildComponent();
            }
        }else {
            if (APP["noSession"].APP["defaultIndex"] == $this->module.$this->subModule) {
                $this->buildComponent();
            }else {
                $this->module = "error";
                $this->subModule = "session-required";
                $this->buildComponent();
            }
        }
    }

    private function buildComponent(): void
    {
        $component = new Component($this->module, $this->subModule);
    }
}
