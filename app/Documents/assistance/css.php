<?php
//Estilos de el formato de asistencia
return <<<HTML
<style>
    * {
        font-size: 11;
    }

    table,
    td,
    th {
        border: 1px solid #b8b8b8;
    }

    table {
        border-collapse: collapse;
    }

    td,
    th {
        vertical-align: middle;
        padding: 2px;
        height: 15px;
    }

    .logo {
        margin: 2px;
    }

    .text-center {
        text-align: center;
    }

    .text-top {
        vertical-align: top;
    }

    .text-right {
        text-align: right;
    }

    .text-left {
        text-align: left;
    }
</style>
HTML;
