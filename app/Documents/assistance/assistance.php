<?php

//Se incluyen los estilos del formato de asistencia
$css = include "app/Documents/assistance/css.php";

$count = 1;
$assistants = "";
//Se define el numero de asistentes por pagina, despues de que la primer pagina llega a su limite
$assistansPerPage = 25;
//Se cuenta la cantidad de asistentes
$assistantsLength = count($data["assistants"]);

//Se crea la lista de minimo 15 repeticiones de la primer pagina
for ($i = 0; $i < 15; $i++) {
    if (isset($data["assistants"][$i])) {
        $assistant = $data["assistants"][$i];

        //Se limita la longitud del nombre completo del empleado
        $assistantFullName = strLimit($assistant['fullName'], 36);
        //Se limita la longitud del nombre del centro de costos
        $assistantCostCenter = strLimit($assistant['costCenter'], 28);

        $assistants .= <<<HTML
        <tr>
            <td class='text-center'> {$count}</td>
            <td>{$assistantFullName}</td>
            <td>{$assistant['code']}</td>
            <td>{$assistantCostCenter}</td>
            <td></td>
        </tr>
HTML;
    } else {
        $assistants .= <<<HTML
        <tr>
            <td class='text-center'> {$count}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
HTML;
    }
    $count++;
}

//Se diseña el encabezado de la primer pagina y se concatena la primer lista de asistentes que tiene una longitud establecida de 15
$format = <<<HTML
<page footer="page">
    <table class="text-spaces">
        <tbody>
            <tr>
                <td class="text-center" style="width: 160px;height: 40px;"><img class="logo" src="app-public/assets/images/logo-sm-pdf.jpg" height="40" width="auto"></td>
                <td class="text-center" style="width: 400px;" colspan="2"> <b> REGISTRO DE ASISTENCIA A CAPACITACIONES </b> </td>
                <td class="text-center" style="width: 160px;"><b> FO-GH - 24 </b> </td>
            </tr>
            <tr>
                <td colspan="3">CENTRO DE PRODUCCIÓN: {$data["productionCenter"]}</td>
                <td>CODIGO: {$data["code"]}</td>
            </tr>
            <tr>
                <td>FECHA: {$data["date"]}</td>
                <td style="width: 100px;">HORA INICIO: {$data["startTime"]}</td>
                <td style="width: 100px;">HORA FIN: {$data["endTime"]}</td>
                <td>TOTAL HORAS: {$data["plannedHours"]}</td>
            </tr>
            <tr>
                <td colspan="3">LUGAR: {$data["place"]}</td>
                <td class="text-center">UBICACION A-Z</td>
            </tr>
            <tr>
                <td colspan="3">TÍTULO CAPACITACIÓN: {$data["theme"]}</td>
                <td class="text-center">{$data["az"]}</td>
            </tr>
            <tr>
                <td colspan="3">PROCESO: {$data["process"]}</td>
                <td class="text-center">PERSONAS A EVALUAR</td>
            </tr>
            <tr>
                <td colspan="3">DIRIGIDO A: {$data["addressed"]}</td>
                <td class="text-center">{$data["percentEvaluate"]}% - {$data["numPeople"]} Persona(s)</td>
            </tr>
            <tr>
                <td colspan="4">MATERIAL DIDACTICO: {$data["materials"]}</td>
            </tr>
            <tr>
                <td class="text-center" colspan="2"><b>CONTENIDOS</b></td>
                <td class="text-center" colspan="2"><b>OBJETIVO</b></td>
            </tr>
            <tr>
                <td class="text-left text-top" colspan="2" style="height: 90px; min-width: 200px;max-width: 200px;"><span style="padding:0px -50px;">{$data["content"]}</span></td>
                <td class="text-left text-top" colspan="2" style="min-width: 200px;max-width: 200px;">{$data["objective"]}</td>
            </tr>
        </tbody>
    </table>
    <table>
        <tbody>
            <tr>
                <td style="width: 18px;"><b>No.</b></td>
                <td style="width: 250px;"><b>Apellidos y Nombres</b></td>
                <td class="text-center" style="width: 130px;"><b>Cédula</b></td>
                <td class="text-center" style="width: 130px;"><b>Área</b></td>
                <td class="text-center" style="width: 170px;"><b>Firma</b></td>
            </tr>
            {$assistants}
        </tbody>
    </table>
    <table style="margin-top: 50px;">
      <tbody>
         <tr>
             <td style="width: 366px;" >NOMBRE DEL RESPONSABLE DEL PROCESO: {$data["responsableName"]}</td>
             <td style="width: 365px;">NOMBRE DEL CAPACITADOR: {$data["trainerName"]}</td>
         </tr>
         <tr>
             <td>FIRMA DEL RESPONSABLE DEL PROCESO: ____________________</td>
             <td>PROFESIÓN/ CARGO DEL CAPACITADOR: {$data["trainerPosition"]}</td>
         </tr>
         <tr>
             <td></td>
             <td>FIRMA DEL CAPACITADOR: ____________________</td>
         </tr>
      </tbody>
    </table>
    <page_footer>
    </page_footer>
</page>
HTML;

//Se concatena los estilos y la primer pagina
$document = $css . $format;

$countLess = $count - 1;
$head = "";
$body = "";
$footer = "";
$assistantsList = "";

//Se valida si hay mas de 15 asistentes, para continuar con las siguientes paginas
if ($assistantsLength > 15) {
    while ($countLess < $assistantsLength) {
        //Se diseña el encabezado para las siguientes paginas, con menos información
        $head = <<<HTML
        <page footer="page">
            <table>
                <tbody>
                    <tr>
                        <td class="text-center" style="width: 160px;height: 40px;"><img class="logo" src="app-public/assets/images/logo-sm-pdf.jpg" height="40" width="auto"></td>
                        <td class="text-center" style="width: 400px;" colspan="2"> <b> REGISTRO DE ASISTENCIA A CAPACITACIONES </b> </td>
                        <td class="text-center" style="width: 160px;"><b> FO-GH - 24 </b> </td>
                    </tr>
                    <tr>
                        <td colspan="3">CENTRO DE PRODUCCIÓN: {$data["productionCenter"]}</td>
                        <td>CODIGO: {$data["code"]}</td>
                    </tr>
                    <tr>
                        <td>FECHA: {$data["date"]}</td>
                        <td style="width: 100px;">HORA INICIO: {$data["startTime"]}</td>
                        <td style="width: 100px;">HORA FIN:</td>
                        <td>TOTAL HORAS: {$data["plannedHours"]}</td>
                    </tr>
                    <tr>
                        <td colspan="3">LUGAR: {$data["place"]}</td>
                        <td class="text-center">UBICACION A-Z</td>
                    </tr>
                    <tr>
                        <td colspan="3">TÍTULO CAPACITACIÓN: {$data["theme"]}</td>
                        <td class="text-center">{$data["az"]}</td>
                    </tr>
                    <tr>
                        <td colspan="4">PROCESO: {$data["process"]}</td>
                    </tr>
                </tbody>
            </table>
            <table style="margin-top: 30px;">
                <tbody>
                    <tr>
                        <td style="width: 18px;"><b>No.</b></td>
                        <td style="width: 250px;"><b>  Apellidos y Nombres</b></td>
                        <td class="text-center" style="width: 110px;"><b>Cédula</b></td>
                        <td class="text-center" style="width: 150px;"><b>Área</b></td>
                        <td class="text-center" style="width: 170px;"><b>Firma</b></td>
                    </tr>
HTML;
        //Se crea la lista de asitentes
        for ($j=$countLess; $j < $countLess + $assistansPerPage; $j++) {
            if (isset($data["assistants"][$j])) {
                $assistant = $data["assistants"][$j];

                $assistantFullName = strLimit($assistant['fullName'], 36);
                $assistantCostCenter = strLimit($assistant['costCenter'], 28);

                $body .= <<<HTML
                <tr>
                    <td class='text-center'> {$count}</td>
                    <td>{$assistantFullName}</td>
                    <td>{$assistant['code']}</td>
                    <td>{$assistantCostCenter}</td>
                    <td></td>
                </tr>
HTML;
            } else {
                break;
            }
            $count++;
        }
        $footer = <<<HTML
                </tbody>
            </table>
            <page_footer>
            </page_footer>
        </page>
HTML;
        $assistantsList .= $head . $body . $footer;
        $body = "";
        $countLess = $countLess + $assistansPerPage;
    }
}

//A las primer pagina se le concatena si existen las paginas siguientes
$document .= $assistantsList;

return $document;
