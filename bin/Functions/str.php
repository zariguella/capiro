<?php

function strReplaceAssoc(array $replace, $subject): string{

   return str_replace(array_keys($replace), array_values($replace), $subject);
}

function strLimit(string $string, int $limit): string
{
    $points = "";
    if (strlen($string) > $limit) {
        $points = "-";
    }
    $newString = substr($string, 0, $limit);
    return $newString . $points;
}
