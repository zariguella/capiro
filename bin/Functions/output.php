<?php

function redirTo(string $url, string $location = "local"): void
{
    $finalUlrl = APP["baseUrl"].$url;
    header("location: {$finalUlrl}");
}

function endAjax(string $status, string $msg, array $data = []): void
{
    $jsonBuild = [
        "status" => $status,
        "msg" => $msg,
        "data" => $data
    ];
    echo arrayToJson($jsonBuild);
    exit;
}

function endArray(string $status, string $msg, array $data = []): array
{
    $arrayBuild = [
        "status" => $status,
        "msg" => $msg,
        "data" => $data
    ];
    return $arrayBuild;
}

function endComponent(string $status, string $msg = ""): void
{
    exit($msg);
}

function endApp(string $msg = ""): void
{
    echo $msg;
    $layout = new Layout("error", "fatal", false, false, false);
    exit;
}
