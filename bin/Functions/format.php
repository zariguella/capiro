<?php

function formatController(string $data): string
{
    $strLower = strtolower($data);
    $strArray = explode("-", $strLower);
    $newString = "";
    foreach ($strArray as $value) {
        $newString = $newString.ucfirst($value);
    }

    return $newString;
}

function formatAction(string $data): string
{
    $strLower = strtolower($data);
    $strArray = explode("-", $strLower);
    $newString = "";
    $count = 0;
    foreach ($strArray as $value) {
        if ($count !== 0) {
            $newString = $newString.ucfirst($value);
        } else {
            $newString = $newString.$value;
        }
        $count++;
    }
    return $newString;
}

function formatModel(string $data): string
{
    $formated = ucfirst(strtolower($data));

    return $formated;
}

function formatView(string $data): string
{
    $formated = strtolower($data);

    return $formated;
}
