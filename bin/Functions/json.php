<?php

function jsonToArray(string $data = "", string $type = "string", bool $validate = true): array
{
    $finalJson = "";
    $temporalJson = "";

    switch ($type) {
        case 'file':
            $temporalJson = file_get_contents($data);
            $finalJson = json_decode($temporalJson, true);
            break;
        case 'string':
            $finalJson = json_decode($data, true);
            break;
        default:
            $finalJson = json_decode($data, true);
            break;
    }

    if ($validate) {
        if (json_last_error()) {
            endApp("Error al procesar el json: ".json_last_error_msg());
        }
    }

    return $finalJson;
}

function arrayToJson(array $array, bool $validate = true): string
{
    $temporalJson = json_encode($array);

    if (json_last_error()) {
        endApp("Error al procesar el json: ".json_last_error_msg());
    }

    return $temporalJson;
}
