<?php
/**
 * Conexión con base de datos nomina
 */
class RosterConn
{

    public $conn = null;

 	public function __construct()
    {
		try {
			$this->conn = $this->getConnection();

		} catch(PDOException $ex) {
            endAjax("error", "No se pudo conectar a la base de datos ... " . $ex->getMessage());
		}
	}

	private function getConnection()
    {
        $DB = DB["Roster"];

        $opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];

        $dsd = "{$DB['driver']}:Server={$DB['serverIP']};Database={$DB['database']}";
        $pdo = new PDO($dsd, $DB["user"], $DB["pass"], $opt);

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $pdo;
	}
}
