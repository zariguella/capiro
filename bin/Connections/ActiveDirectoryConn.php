<?php
/**
 * Conexión con el directorio activo
 */
class ActiveDirectoryConn
{
    public $conn = null;
    public $reader;

    public function __construct()
    {
        try {
            $this->conn = $this->getConnection();
        } catch (\Exception $e) {
            endAjax("error", "No se pudo conectar a Directorio Activo.".$e);
        }
    }

    private function getConnection()
    {
        $DB = DB["ActiveDirectory"];
        $ldap = ldap_connect($DB["server"]);
        if (!$ldap) {
            endAjax("error", "No es posible conectarse al servidor.");
        }
        return $ldap;
    }

    protected function bind($nick, $pass)
    {
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        @$bind = ldap_bind($this->conn, $nick, $pass);
        return $bind;
    }
}
