<?php
/**
 * Clase de usuario, encarga de almacenar los permisos y la información basica del usario
 */
class User
{

    private $code;
    private $name, $lastName, $type, $typeName;
    private $productionCenter, $costCenter;
    private $permission, $token;

    //Carga la información perteneciente al usuario
    public function __construct(array $data)
    {
        $this->code = $data["code"];
        $this->name = $data["name"];
        $this->lastName = $data["lastName"];
        $this->type = $data["type"];
        $this->typeName = $data["typeName"];
        $this->productionCenter = $data["productionCenter"];
        $this->costCenter = $data["costCenter"];
        $this->loadPermission();
    }

    //Carga los permisos dependiendo del tipo de usuario
    private function loadPermission(): void
    {
        $dictionaries = PATH["dictionaries"];
        $this->permission = jsonToArray("{$dictionaries}permission-{$this->type}.json", "file");
    }

    public function controllerPermission(string $controller, string $action): bool
    {
        if (isset($this->permission["controller"][$controller])) {
            return in_array($action, $this->permission["controller"][$controller]);
        }
        return false;
    }

    public function viewPermission(string $module, string $subModule): bool
    {
        if (isset($this->permission["view"][$module])) {
            return in_array($subModule, $this->permission["view"][$module]);
        }
        return false;
    }

    public function componentPermission(string $module, string $subModule): bool
    {
        if (isset($this->permission["component"][$module])) {
            return in_array($subModule, $this->permission["component"][$module]);
        }
        return false;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getProductionCenter(): int
    {
        return $this->productionCenter;
    }

    public function getInfo(): array
    {
        return [
           "code" => $this->code,
           "name" => $this->name,
           "lastName" => $this->lastName,
           "type" => $this->type,
           "typeName" => $this->typeName,
           "productionCenter" => $this->productionCenter,
           "costCenter" => $this->costCenter
       ];
    }
}
