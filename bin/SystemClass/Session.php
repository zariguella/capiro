<?php
/**
 * Clase para el manejo de sesiónes de php
 */
class Session
{
    //Valdia la existencia de una sesión
    public static function check(string $name): bool
    {
        if (isset($_SESSION[$name])) {
            return true;
        }else {
            return false;
        }
    }

    public static function getByName(string $name)
    {
        return $_SESSION[$name];
    }

    public static function set(string $name, $data): void
    {
        $_SESSION[$name] = $data;
    }

    public static function start(): void
    {
        session_start();
    }

    public static function destroyAll(): void
    {
        session_unset();
        session_destroy();
    }

    public static function destroyTotalAll(): void
    {
        $this->destroyAll();
        session_write_close();
        setcookie(session_name(),'',0,'/');
    }

    public static function use(string $name)
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        } else {
            return null;
        }
    }
}
