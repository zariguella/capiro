<?php
/**
 * Clase para el manejo de las Cookies
 * No esta en uso
 */
class Cookie
{
    public static function add(string $name, $val, int $time = 3600): void
    {
        setcookie($name, $val, time() + $time);
    }

    public static function set(string $name, $val, int $time = 3600): void
    {
        setcookie($name, $val, time() + $time);
    }

    public static function get(string $name)
    {
        return $_COOKIE[$name];
    }

    public static function delete(string $name): void
    {
        unset($_COOKIE[$name]);
    }

    public static function isset(string $name): bool
    {
        if (isset($_COOKIE[$name])) {
            return true;
        } else {
            return false;
        }
    }
}
