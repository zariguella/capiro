<?php
/**
 * Clase encargada de ingresar los registros de auditoría
 */
class Audit
{

    private $errors, $types;

    //Se definen los tipos de auditoría
    public function __construct()
    {
        $this->errors = "";
        $this->types = [
            "INSERT" => 1,
            "UPDATE" => 2,
            "DELETE" => 3,
            "SIGNIN" => 4
        ];
        new LoadModel("Audit");
    }

    /**
     * Crea un nuevo registro de auditoría, dependiendo de su tipo
     * @param  string $type     [Tipo de la auditoría]
     * @param  string $table    [Tabla a la que hace referencia la auditoría]
     * @param  array  $inserted [Información insertada]
     * @param  array  $deleted  [Información eliminada]
     * @return bool             [Estatus de la ejecución]
     */
    public function create(string $type, string $table = null, array $inserted = [], array $deleted = []): bool
    {
        $gump = new GUMP();
        $gump->validation_rules(array(
                "type" => "required"
        ));

        $validatedData = $gump->run(["type" => $type]);
        if (!$validatedData) {
            $this->errors = $gump->get_readable_errors(true);
            return false;
        }

        if (!isset($this->types[$type])) {
            $this->errors = "El tipo no existe.";
            return false;
        }

        $auditModel = new AuditModel();
        $audiCode = $auditModel->getLastCode() + 1;
        $userCode = Session::use("user")->getCode();
        $dateTime = $auditModel->getDateTime();

        $structure = [
            "code" => $audiCode,
            "user" => $userCode,
            "date" => $dateTime,
            "type" => $this->types[$type],
            "table" => $table,
            "inserted" => arrayToJson($inserted),
            "deleted" => arrayToJson($deleted)
        ];

        if ($auditModel->create($structure)) {
            return true;
        }else {
            $this->errors .= "Error al insertar";
            return false;
        }
    }

    public function getErrors(): string
    {
        return $this->errors;
    }

    public function getLastSignin(string $code): string
    {
        $auditModel = new AuditModel();
        $lastSignin = $auditModel->getLastSignin($code, $this->types["SIGNIN"]);
        return $lastSignin;
    }
}
