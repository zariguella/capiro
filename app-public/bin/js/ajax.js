function request(data) {
    var obj = {
        url: "",
        method: "GET",
        dataType: "",
        beforeSend: function() {
            ppAlert("icon", "loading");
            $(".control-enabled").prop("disabled", true);
        }
    };
    switch (data.type) {
        case "ajax":
            obj.url = obj.url + data.type;
            if (typeof data.data !== 'undefined') {
                obj.method = "POST";
                obj.data = data.data;
            }
            obj.dataType = "json";
            break;
        case "component":
            obj.url = obj.url + data.type;
            obj.dataType = "html";
            break;
        default:
            console.log("Error. Tipo de peticion no definida");
            return;
    }
    obj.url = obj.url +"/"+ data.url;
    $.ajax(obj)
    .done(function(response){
        ppAlert("icon", "success");
        $(".control-enabled").prop("disabled", false);
        //console.log(response);
        if (data.type=="ajax") {
            if (response.status=="redir") {
                window.location=response.data.url;
            }else if (response.status=="success") {
                if (obj.method=="GET") {
                    ppAlert("alert", "success", {msg: response.msg});
                    coordinador("Pepe", response.data);
                    //objUserNew.lists=response.data;
                }else {
                    ppAlert("alert", "success", {msg: response.msg});
                    return;
                }
            }else{
                ppAlert("alert", response.status, {msg: response.msg});
                return;
            }
        }else {
            return $("#" + data.to).html(response);
        }
    }).fail(function(jqXHR, textStatus){
        $(".control-enabled").prop("disabled", false);
        ppAlert("alert", "noConn", {msg: textStatus});
    });

}
