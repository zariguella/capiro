function ValidatorRemoveFeed(pId) {
    var className = "validator";
    $("#" + pId + " ." + className).each(function() {
        var myInput = this;
        $(myInput).removeClass("is-invalid");
        $(myInput).removeClass("is-valid");
    });
}

function Validator(pId) {
    var className = "validator";
    var validated = true;
    $("#" + pId + " ." + className).each(function() {
        var state = true;
        var str = $(this).data("validator");
        var conditions = str.split("|");
        var myInput = this;
        conditions.forEach(function(value) {
            switch (value) {
                case "noEmpty":
                    if ($(myInput).val() == undefined || $(myInput).val() == "" || $(myInput).val() == null) {
                        state = false;
                    }
                    console.log("yep");
                    break;
                case "isEmpty":
                    if (validator.isEmpty($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "alpha":
                    if (!validator.isAmail($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "alphaNumeric":
                    if (!validator.isAlphanumeric($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "boolean":
                    if (!validator.isBoolean($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "decimal":
                    if (!validator.isDecimal($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "email":
                    if (!validator.isEmail($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "float":
                    if (!validator.isFloat($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "JSON":
                    if (!validator.isJSON($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "lowercase":
                    if (!validator.isLowercase($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "numeric":
                    if (!validator.isNumeric($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "url":
                    if (!validator.isUrl($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "uppercase":
                    if (!validator.isUppercase($(myInput).val())) {
                        state = false;
                    }
                    break;
                case "length":
                    var len = {
                        min: 0,
                        max: undefined
                    }
                    if (typeof $(myInput).data("minlen") !== undefined) {
                        len.min = $(myInput).data("minlen");
                    }
                    if (typeof $(myInput).data("maxlen") !== undefined) {
                        len.max = $(myInput).data("maxlen");
                    }
                    if (!validator.isLength($(myInput).val(), len)) {
                        state = false;
                    }
                    break;
            }
            if (state == false) {
                validated = false;
                $(myInput).removeClass("is-valid");
                $(myInput).addClass("is-invalid");
            } else {
                $(myInput).removeClass("is-invalid");
                $(myInput).addClass("is-valid");
            }
        })
    });
    return validated;
}
