function buildOverallNav(pUrl) {

    var modules, lastModule, operatedLastModule, finalPath, previousPath, count = 0,
        layout, home = "home", title = "",
        noFound = "not-found",
        esText = "",
        tempPreviousPath;

    layout = {
        backButton: '',
        start: '<span class="align-middle ml-2">',
        end: '</span>',
        navLinks: ''
    };

    //Dividimos la string, teniendo como carecter para dividir /
    modules = pUrl.split("/").filter(Boolean);
    //Obtenemos la ultima posición del array

    //Eliminamos el slash, si hay alguno, al comienzo de la cadena
    if (pUrl.charAt(0) == "/") {
        lastModule = pUrl.substr(1);
    }else {
        lastModule = pUrl;
    }

    //Verificar si la string dividida existe
    if (lastModule !== undefined && lastModule !== "" && lastModule !== null) {

        //Verificamos si hay un signo de ? que indica variables get
        operatedLastModule = lastModule.split("#");
        //Obtenemos una string sin las variables
        lastModule = operatedLastModule[0];

        //Verificamos si hay un signo de # que indica variables get
        operatedLastModule = lastModule.split("?");
        //Obtenemos una string sin las variables
        lastModule = operatedLastModule[0];

        //Verificamos si la string ejem: user, esta definida en la lista de urls
        if (MyApp.nav[lastModule] !== undefined) {
            title = MyApp.nav[lastModule].esTitle;
            do {
                //Verificamos si la url tiene un url previa
                if (MyApp.nav[lastModule].previousPath !== undefined) {
                    //Le asignamos la url previa
                    previousPath = MyApp.nav[lastModule].previousPath;
                    //Asignacion temporal de la url previa
                    tempPreviousPath = previousPath;
                    //Url final y completa
                    finalPath = lastModule;
                    //Validamos si es la primer vez en el ciclo
                    if (count === 0) {
                        //Validamos si la url previa es url definida como casa
                        if (previousPath === home) {
                            finalPath = MyApp.nav.home.path;
                            tempPreviousPath = MyApp.nav.home.path;
                        }
                        layout.backButton = '<a class="router-link btn btn-secondary btn-sm mr-2" href="' + tempPreviousPath + '" data-linkto="mainContent"><i class="fas fa-angle-left"></i> Atras</a>';
                        layout.navLinks = '<a class="text-muted">' + MyApp.nav[lastModule].esText + '</a>';
                    } else {
                        layout.navLinks = '<a class="router-link" href="' + finalPath + '" data-linkto="mainContent">' + MyApp.nav[lastModule].esText + '</a> <i class="fas fa-angle-right text-muted"></i> ' + layout.navLinks;
                    }
                    lastModule = previousPath;
                    count++;
                } else {
                    esText = MyApp.nav[lastModule].esText;
                    if (lastModule === home) {
                        lastModule = MyApp.nav.home.path;
                    }
                    finalPath = lastModule;
                    if (count === 0) {
                        layout.navLinks = '<a class="text-muted">' + MyApp.nav[lastModule].esText + '</a>';
                    } else {
                        layout.navLinks = '<a class="router-link" href="' + finalPath + '" data-linkto="mainContent">' + esText + '</a> <i class="fas fa-angle-right text-muted"></i> ' + layout.navLinks;
                    }
                    break;
                }
            } while (count < 10);
        } else {
            layout.backButton = '<a class="router-link btn btn-secondary btn-sm mr-2" href="' + MyApp.nav.home.path + '" data-linkto="mainContent"><i class="fas fa-angle-left"></i> Atras</a>';
            layout.navLinks = '<a class="text-muted">' + MyApp.nav[noFound].esText + '</a>';
            title = MyApp.nav[noFound].esTitle;
        }
    } else {
        layout.navLinks = '<a class="text-muted">' + MyApp.nav[MyApp.nav.home.path].esText + '</a>';
        title = MyApp.nav[MyApp.nav.home.path].esTitle;
    }
    document.title = title + ' - ' + MyApp.title;
    $("#navContainer").html(layout.backButton + layout.start + layout.navLinks + layout.end);
}
