var JSONnav = {
    "home": {
        "path": "calendar",
        "esText": "<i class='fas fa-home mr-2'></i>"
    },
    "signin": {
        "esText": "Inicio de Sesión",
        "esTitle": "Inicio de Sesión"
    },
    "user": {
        "esText": "Usuario",
        "esTitle": "Usuario",
        "previousPath": "home",
    },
    "user/manage": {
        "esText": "Selección Modulo a Gestionar",
        "esTitle": "Selección Modulo a Gestionar",
        "previousPath": "user",
    },
    "user/manage-items": {
        "esText": "Gestionar Items",
        "esTitle": "Gestionar Items",
        "previousPath": "user/manage"
    },
    "user/manage-users": {
        "esText": "Gestionar Usuarios",
        "esTitle": "Gestionar Usuarios",
        "previousPath": "user/manage"
    },
    "user/account": {
        "esText": "Cuenta",
        "esTitle": "Cuenta",
        "previousPath": "user"
    },
    "user/notifications": {
        "esText": "Notificaciones",
        "esTitle": "Notificaciones",
        "previousPath": "user"
    },
    "user/config": {
        "esText": "Configuraciones",
        "esTitle": "Configuraciones",
        "previousPath": "user"
    },
    "calendar": {
        "otherName": "",
        "esTitle": "Calendario",
        "esText": "<i class='far fa-calendar-alt mr-2'></i> Calendario"
    },
    "training": {
        "esText": "<i class='fas fa-chalkboard-teacher mr-2'></i> Capacitaciones",
        "esTitle": "Capacitaciones"
    },
    "training/manage": {
        "esText": "Gestionar Capacitaciones",
        "esTitle": "Gestionar Capacitaciones",
        "previousPath": "training"
    },
    "training/end": {
        "esText": "Finalizar Capacitación",
        "esTitle": "Finalizar Capacitación",
        "previousPath": "training"
    },
    "training/view": {
        "esText": "<i class='fas fa-eye mr-2'></i> Ver Capacitación",
        "esTitle": "Ver capacitación"
    },
    "training/edit-in-execution": {
        "esText": "Actualizar Capacitación en Ejecución",
        "esTitle": "Actualizar Capacitación en Ejecución",
        "previousPath": "training/manage"
    },
    "reports": {
        "esText": "<i class='fas fa-book mr-2'></i> Reportes",
        "esTitle": "Reportes"
    },
    "reports/production-center": {
        "esText": "Indicadores por Centro de Producción",
        "esTitle": "Indicadores por Centro de Producción",
        "previousPath": "reports"
    },
    "reports/effectiveness": {
        "esText": "Indicadores de Efectividad",
        "esTitle": "Indicadores de Efectividad",
        "previousPath": "reports"
    },
    "reports/coverage": {
        "esText": "Indicadores de Cobertura",
        "esTitle": "Indicadores de Cobertura",
        "previousPath": "reports"
    },
    "reports/totals": {
        "esText": "Indicadores Totales",
        "esTitle": "Indicadores Totales",
        "previousPath": "reports"
    },
    "reports/employee": {
        "esText": "Reporte Empleado",
        "esTitle": "Reporte Empleado",
        "previousPath": "reports"
    },
    "reports/training": {
        "esText": "Reportes Capacitación",
        "esTitle": "Reportes Capacitación",
        "previousPath": "reports"
    },
    "reports/training-effectiveness": {
        "esText": "Efectividad Por Capacitación",
        "esTitle": "Efectividad Por Capacitación",
        "previousPath": "reports"
    },
    "help": {
        "esText": "<i class='far fa-question-circle'></i> Ayuda",
        "esTitle": "Ayuda",
        "previousPath": "home"
    },
    "not-found": {
        "esText": "Lugar No Encontrado",
        "esTitle": "Lugar No Encontrado"
    },
    "error/404": {
        "esText": "Ubicación Totalmente Desconocida",
        "esTitle": "Ubicación Totalmente Desconocida",
        "previousPath": "home"
    }
};
var urlSub;
var MyApp = {
    nav: JSONnav
};

function loadLibraries() {
    $('[data-toggle="popover"]').popover();
    $('.popover-dismiss').popover({
        trigger: 'focus'
    });
    $('[data-toggle="tooltip"]').tooltip();
}

/*function OnlyNumbers(string) { //Solo numeros
    var out = '';
    if (string < 0 || string > 100) {
        out = string.slice(0,2);
        return out;
    }else {
        var filter = '1234567890'; //Caracteres validos
        //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
        for (var i = 0; i < string.length; i++) {
            if (filter.indexOf(string.charAt(i)) != -1) {
                //Se añaden a la salida los caracteres validos
                out += string.charAt(i);
            }
        }
        //Retornar valor filtrado
        return out;
    }

}*/

function numbers(e) {
    if (e < 0 || e > 100) {
        console.log("entro");
        e = null;
        return e;
    }else if( isNaN(e) ) {
        return null;
    }else {
        console.log('bye');
        return e;
    }
}

function setViewForUserType() {
    userType = MyApp.userType;
    if (userType == undefined) {
        userType = 1;
    }
    $(".notus" + userType).remove();
}

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,',
      template =
      '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
      base64 = function(s) {
         return window.btoa(unescape(encodeURIComponent(s)));
      },
      format = function(s, c) {
         return s.replace(/{(\w+)}/g, function(m, p) {
            return c[p];
         });
      };
  return function(table, name) {
      if (!table.nodeType) table = document.getElementById(table);
      var ctx = {
         worksheet: name || 'Worksheet',
         table: table.innerHTML
      };
      window.location.href = uri + base64(format(template, ctx));
  };
})();
