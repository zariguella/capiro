var ppAlertTime, ppActivedIcon = "fas fa-check", ppActivedIconColor = "success", itemColor = "badge-";
function ppAlert(type, status, data = "", sec = 5) {
    var ppAlertData = {
        alert: {
            error: { color: "danger", title: "Error!", icon: "fas fa-exclamation-circle", msg: "Ha ocurrido un error inesperado." },
            success: { color: "success", title: "Éxito!", icon: "fas fa-check-circle", msg: "Se ha realizado correctamente." },
            info: { color: "info", title: "Información!", icon: "fas fa-info-circle", msg: "Información nueva." },
            warning: { color: "warning", title: "Cuidado!", icon: 'fas fa-exclamation-circle', msg: "Cuidado, puede haber un error." },
            noConn: { color: "secondary", title: "Sin conexión", icon: 'fas fa-plug', msg: "No hay conexión con el servidor." }
        },
        icon: {
            loading: { icon: 'fas fa-circle-notch pp-fa-spin', color: "success", title: "Cargando..." },
            error: { color: "danger", title: "Error!", icon: "fas fa-times" },
            success: { color: "success", title: "Ok!", icon: "fas fa-check" },
            info: { color: "info", title: "Información!", icon: "fas fa-info" },
            warning: { color: "warning", title: "Cuidado!", icon: 'fas fa-exclamation' },
            noConn: { color: "warning", title: "Sin conexión", icon: 'fas fa-plug' }
        }
    };
    if (type == "alert") {
        ppCloseAlert();
        nData = $.extend(ppAlertData.alert[status], data);
        var html = '<span class="float-right text-secondary" onclick="ppCloseAlert()"><i class="fas fa-times"></i></span><span class="pp-alert-title text-' + nData.color + ' mr-4"><i class="' + nData.icon + ' mr-2"></i>' + nData.title + '</span><br><span class="pp-alert-text"> ' + nData.msg + ' </span>';
        $("#pp-alert-span").attr("data-content", html);
        ppShowAlert(sec);
    }
    $("#pp-alert-icon").removeClass(ppActivedIcon);
    $("#pp-alert-span").removeClass(itemColor + ppActivedIconColor);
    ppActivedIcon = ppAlertData.icon[status].icon;
    ppActivedIconColor = ppAlertData.icon[status].color;
    $("#pp-alert-icon").addClass(ppActivedIcon);
    $("#pp-alert-span").addClass(itemColor + ppActivedIconColor);
    $('#pp-alert-tooltip').tooltip('dispose');
    $("#pp-alert-tooltip").tooltip({title: ppAlertData.icon[status].title});
}

function ppShowAlert(sec) {
    $('#pp-alert-span').popover('show');
    ppAlertTime = setTimeout(ppCloseAlert, (sec*1000));
}

function ppCloseAlert() {
    $('#pp-alert-span').popover('hide');
    clearTimeout(ppAlertTime);
}
