-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2019 a las 23:39:50
-- Versión del servidor: 5.7.24-log
-- Versión de PHP: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `training`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `addressed`
--

CREATE TABLE `addressed` (
  `addr_code` smallint(6) NOT NULL,
  `addr_name` varchar(50) NOT NULL,
  `addr_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `addressed`
--

INSERT INTO `addressed` (`addr_code`, `addr_name`, `addr_state`) VALUES
(1, 'todo el personal', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `assistant`
--

CREATE TABLE `assistant` (
  `assi_training_code` int(11) NOT NULL,
  `assi_employee_code` varchar(15) NOT NULL,
  `assi_cost_center` varchar(50) NOT NULL,
  `assi_assistant` tinyint(1) NOT NULL DEFAULT '0',
  `assi_score` double(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `assistant`
--

INSERT INTO `assistant` (`assi_training_code`, `assi_employee_code`, `assi_cost_center`, `assi_assistant`, `assi_score`) VALUES
(1, '1001546389', 'Produccion', 1, 100.00),
(1, '1007706278', 'Produccion', 0, NULL),
(1, '1022033617', 'Produccion', 0, NULL),
(1, '1022034553', 'Produccion', 0, NULL),
(1, '1022035622', 'Produccion', 0, NULL),
(2, '1001546389', 'Produccion', 1, 100.00),
(2, '1007706278', 'Produccion', 0, NULL),
(2, '1022033617', 'Produccion', 0, NULL),
(2, '1022034553', 'Produccion', 0, NULL),
(2, '1022035622', 'Produccion', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audit`
--

CREATE TABLE `audit` (
  `audi_code` bigint(20) NOT NULL,
  `audi_user` varchar(15) NOT NULL,
  `audi_date` datetime NOT NULL,
  `audi_type` tinyint(4) NOT NULL,
  `audi_table` varchar(40) DEFAULT NULL,
  `audi_inserted` text,
  `audi_deleted` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audit`
--

INSERT INTO `audit` (`audi_code`, `audi_user`, `audi_date`, `audi_type`, `audi_table`, `audi_inserted`, `audi_deleted`) VALUES
(1, '1036938697', '2018-12-11 14:06:44', 4, NULL, '[]', '[]'),
(2, '1036938697', '2018-12-11 14:13:59', 1, 'training', '{\"trai_code\":1,\"trai_target_people\":\"5\",\"trai_date_year\":2018,\"trai_date_week\":50,\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":2,\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\"}', '[]'),
(3, '1036938697', '2018-12-11 14:14:18', 4, NULL, '[]', '[]'),
(4, '1036938697', '2018-12-11 14:14:31', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_code\":1}', '{\"trai_code\":\"1\",\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(5, '1036938697', '2018-12-11 14:15:10', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"08:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"Esteban\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-1\",\"trai_code\":1}', '{\"trai_code\":\"1\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(6, '1036938697', '2018-12-13 11:01:13', 4, NULL, '[]', '[]'),
(7, '1036938697', '2018-12-13 11:14:15', 4, NULL, '[]', '[]'),
(8, '1036938697', '2018-12-13 11:44:20', 4, NULL, '[]', '[]'),
(9, '1036938697', '2018-12-13 11:47:45', 4, NULL, '[]', '[]'),
(10, '1036938697', '2018-12-13 12:13:12', 4, NULL, '[]', '[]'),
(11, '1036938697', '2018-12-13 13:15:37', 1, 'training', '{\"trai_code\":2,\"trai_target_people\":\"5\",\"trai_date_year\":2018,\"trai_date_week\":50,\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":2,\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\"}', '[]'),
(12, '1036938697', '2018-12-13 13:16:54', 2, 'training', '{\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-14\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(13, '1036938697', '2018-12-13 13:17:20', 2, 'training', '{\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-14\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(14, '1036938697', '2018-12-13 14:12:11', 2, 'training', '{\"trai_state\":3,\"trai_start_time\":\"08:00\",\"trai_end_time\":\"09:05\",\"trai_az_cd\":\"AF-BU-1\",\"trai_code\":1}', '{\"trai_code\":\"1\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-10\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"08:00:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"Esteban\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-1\"}'),
(15, '1036938697', '2018-12-13 14:13:17', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"1\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(16, '1036938697', '2018-12-13 14:28:30', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(17, '1036938697', '2018-12-13 14:28:33', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\"}'),
(18, '1036938697', '2018-12-13 14:29:11', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\"}'),
(19, '1036938697', '2018-12-13 14:30:00', 2, 'training', '{\"trai_state\":3,\"trai_start_time\":\"09:00\",\"trai_end_time\":\"10:00\",\"trai_az_cd\":\"AF-BU-2\",\"trai_code\":2}', '{\"trai_code\":\"2\",\"trai_state\":\"2\",\"trai_target_people\":\"5\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"50\",\"trai_date_format\":\"2018-12-13\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_place\":\"1\",\"trai_percent_evaluate\":\"20\",\"trai_start_time\":\"09:00:00\",\"trai_responsable_name\":\"Juan\",\"trai_trainer_name\":\"perez\",\"trai_trainer_position\":\"1\",\"trai_az_cd\":\"AF-BU-2\"}'),
(20, '1036938697', '2018-12-13 14:37:09', 1, 'training', '{\"trai_code\":3,\"trai_target_people\":\"10\",\"trai_date_year\":2018,\"trai_date_week\":51,\"trai_date_format\":\"2018-12-17\",\"trai_production_center\":2,\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\"}', '[]'),
(21, '1036938697', '2018-12-13 14:37:31', 2, 'training', '{\"trai_state\":\"2\",\"trai_target_people\":\"10\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"51\",\"trai_date_format\":\"2018-12-19\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_code\":3}', '{\"trai_code\":\"3\",\"trai_state\":\"1\",\"trai_target_people\":\"10\",\"trai_date_year\":\"2018\",\"trai_date_week\":\"51\",\"trai_date_format\":\"2018-12-17\",\"trai_production_center\":\"2\",\"trai_process\":\"1\",\"trai_theme\":\"1\",\"trai_planned_hours\":\"2.00\",\"trai_addressed\":\"1\",\"trai_responsable\":\"1\",\"trai_percent_evaluate\":\"20\"}'),
(22, '1036938697', '2019-01-22 15:59:15', 4, NULL, '[]', '[]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audittype`
--

CREATE TABLE `audittype` (
  `auditype_code` tinyint(4) NOT NULL,
  `auditype_abbreviation` varchar(5) NOT NULL,
  `auditype_name` varchar(30) NOT NULL,
  `auditype_description` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `audittype`
--

INSERT INTO `audittype` (`auditype_code`, `auditype_abbreviation`, `auditype_name`, `auditype_description`) VALUES
(1, 'INSE', 'Creación', NULL),
(2, 'UPDA', 'Actualización', NULL),
(3, 'DELE', 'Eliminación', NULL),
(4, 'SIGIN', 'Inicio de Sesión', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification`
--

CREATE TABLE `notification` (
  `nt_code` int(11) NOT NULL,
  `nt_user_code` varchar(15) CHARACTER SET utf8 NOT NULL,
  `nt_type` smallint(6) NOT NULL,
  `nt_date` date NOT NULL,
  `nt_state` tinyint(1) NOT NULL DEFAULT '1',
  `nt_training_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notification_type`
--

CREATE TABLE `notification_type` (
  `ntt_code` smallint(6) NOT NULL,
  `ntt_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ntt_message` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `place`
--

CREATE TABLE `place` (
  `plac_code` smallint(6) NOT NULL,
  `plac_name` varchar(50) NOT NULL,
  `plac_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `place`
--

INSERT INTO `place` (`plac_code`, `plac_name`, `plac_state`) VALUES
(1, 'Comedor', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `process`
--

CREATE TABLE `process` (
  `proc_code` smallint(6) NOT NULL,
  `proc_abbreviation` varchar(4) NOT NULL,
  `proc_name` varchar(50) NOT NULL,
  `proc_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `process`
--

INSERT INTO `process` (`proc_code`, `proc_abbreviation`, `proc_name`, `proc_state`) VALUES
(1, 'BU', 'Buen uso', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productioncenter`
--

CREATE TABLE `productioncenter` (
  `prodcent_code` smallint(6) NOT NULL,
  `prodcent_abbreviation` varchar(4) NOT NULL,
  `prodcent_name` varchar(50) NOT NULL,
  `prodcent_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productioncenter`
--

INSERT INTO `productioncenter` (`prodcent_code`, `prodcent_abbreviation`, `prodcent_name`, `prodcent_state`) VALUES
(2, 'AF', 'C.P. ALHAMBRA', 1),
(3, 'VF', 'C.P. VALLEY', 1),
(4, 'AP', 'C.P. PADUA', 1),
(5, 'PM', 'C.P. PLANTAS MADRES', 1),
(6, 'BC', 'C.P. BOCHICA', 1),
(7, 'BQ', 'C.P. BOUQUETS', 1),
(8, 'ADM', 'SEDE MEDELLIN', 1),
(9, 'SSB', 'C.P. SAN SEBASTIAN', 1),
(10, 'ST', 'C.P. SUSTRATOS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `responsable`
--

CREATE TABLE `responsable` (
  `resp_code` smallint(6) NOT NULL,
  `resp_name` varchar(50) NOT NULL,
  `resp_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `responsable`
--

INSERT INTO `responsable` (`resp_code`, `resp_name`, `resp_state`) VALUES
(1, 'Gestion humana', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teachingmaterial`
--

CREATE TABLE `teachingmaterial` (
  `teacmate_code` smallint(6) NOT NULL,
  `teacmate_name` varchar(50) NOT NULL,
  `teacmate_description` varchar(400) DEFAULT NULL,
  `teacmate_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `teachingmaterial`
--

INSERT INTO `teachingmaterial` (`teacmate_code`, `teacmate_name`, `teacmate_description`, `teacmate_state`) VALUES
(1, 'tijeras', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `theme`
--

CREATE TABLE `theme` (
  `them_code` smallint(6) NOT NULL,
  `them_name` varchar(100) NOT NULL,
  `them_content` varchar(1000) NOT NULL,
  `them_objective` varchar(1000) NOT NULL,
  `them_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `theme`
--

INSERT INTO `theme` (`them_code`, `them_name`, `them_content`, `them_objective`, `them_state`) VALUES
(1, 'Roya Blanca', 'a', 'b', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trainerposition`
--

CREATE TABLE `trainerposition` (
  `traiposi_code` smallint(6) NOT NULL,
  `traiposi_name` varchar(50) NOT NULL,
  `traiposi_state` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `trainerposition`
--

INSERT INTO `trainerposition` (`traiposi_code`, `traiposi_name`, `traiposi_state`) VALUES
(1, 'Operario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `training`
--

CREATE TABLE `training` (
  `trai_code` int(11) NOT NULL,
  `trai_state` tinyint(4) NOT NULL DEFAULT '1',
  `trai_target_people` smallint(6) NOT NULL,
  `trai_date_year` smallint(4) NOT NULL,
  `trai_date_week` tinyint(4) NOT NULL,
  `trai_date_format` date DEFAULT NULL,
  `trai_production_center` smallint(6) NOT NULL,
  `trai_process` smallint(6) NOT NULL,
  `trai_theme` smallint(6) NOT NULL,
  `trai_planned_hours` double(3,2) NOT NULL,
  `trai_addressed` smallint(6) NOT NULL,
  `trai_responsable` smallint(6) NOT NULL,
  `trai_title` varchar(50) DEFAULT NULL,
  `trai_place` smallint(6) DEFAULT NULL,
  `trai_percent_evaluate` tinyint(4) NOT NULL DEFAULT '20',
  `trai_start_time` time DEFAULT NULL,
  `trai_end_time` time DEFAULT NULL,
  `trai_responsable_name` varchar(50) DEFAULT NULL,
  `trai_trainer_name` varchar(50) DEFAULT NULL,
  `trai_trainer_position` smallint(6) DEFAULT NULL,
  `trai_az_cd` varchar(14) DEFAULT NULL,
  `trai_note` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `training`
--

INSERT INTO `training` (`trai_code`, `trai_state`, `trai_target_people`, `trai_date_year`, `trai_date_week`, `trai_date_format`, `trai_production_center`, `trai_process`, `trai_theme`, `trai_planned_hours`, `trai_addressed`, `trai_responsable`, `trai_title`, `trai_place`, `trai_percent_evaluate`, `trai_start_time`, `trai_end_time`, `trai_responsable_name`, `trai_trainer_name`, `trai_trainer_position`, `trai_az_cd`, `trai_note`) VALUES
(1, 3, 5, 2018, 50, '2018-12-10', 2, 1, 1, 2.00, 1, 1, NULL, 1, 20, '08:00:00', '09:05:00', 'Juan', 'Esteban', 1, 'AF-BU-1', NULL),
(2, 3, 5, 2018, 50, '2018-12-13', 2, 1, 1, 2.00, 1, 1, NULL, 1, 20, '09:00:00', '10:00:00', 'Juan', 'perez', 1, 'AF-BU-2', NULL),
(3, 2, 10, 2018, 51, '2018-12-19', 2, 1, 1, 2.00, 1, 1, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trainingstate`
--

CREATE TABLE `trainingstate` (
  `traistat_code` tinyint(4) NOT NULL,
  `traistat_name` varchar(30) NOT NULL,
  `traistat_description` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `trainingstate`
--

INSERT INTO `trainingstate` (`traistat_code`, `traistat_name`, `traistat_description`) VALUES
(1, 'Planeada', NULL),
(2, 'En Ejecución', NULL),
(3, 'Ejecutada', NULL),
(4, 'Desactivada', NULL),
(5, 'Eliminada', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trainingteachingmaterial`
--

CREATE TABLE `trainingteachingmaterial` (
  `traiteacmate_training_code` int(11) NOT NULL,
  `traiteacmate_material_code` smallint(6) NOT NULL,
  `traiteacmate_amount` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_nick` varchar(60) NOT NULL,
  `user_code` varchar(15) NOT NULL,
  `user_production_center` smallint(6) NOT NULL,
  `user_state` tinyint(1) NOT NULL DEFAULT '1',
  `user_type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_nick`, `user_code`, `user_production_center`, `user_state`, `user_type`) VALUES
('test', '1036938697', 3, 1, 3),
('test@capiro.co', '1040049214', 8, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usertype`
--

CREATE TABLE `usertype` (
  `usertype_code` tinyint(4) NOT NULL,
  `usertype_name` varchar(30) NOT NULL,
  `usertype_description` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usertype`
--

INSERT INTO `usertype` (`usertype_code`, `usertype_name`, `usertype_description`) VALUES
(1, 'Auxiliar', NULL),
(2, 'Coordinador', NULL),
(3, 'Administrador', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `addressed`
--
ALTER TABLE `addressed`
  ADD PRIMARY KEY (`addr_code`);

--
-- Indices de la tabla `assistant`
--
ALTER TABLE `assistant`
  ADD PRIMARY KEY (`assi_training_code`,`assi_employee_code`);

--
-- Indices de la tabla `audit`
--
ALTER TABLE `audit`
  ADD PRIMARY KEY (`audi_code`),
  ADD KEY `FK_Audit_User_idx` (`audi_user`),
  ADD KEY `FK_Audit_AuditType_idx` (`audi_type`);

--
-- Indices de la tabla `audittype`
--
ALTER TABLE `audittype`
  ADD PRIMARY KEY (`auditype_code`);

--
-- Indices de la tabla `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`nt_code`),
  ADD KEY `FK_Notificacion_Training_idx` (`nt_training_code`),
  ADD KEY `FK_Notification_NotificationType_idx` (`nt_type`),
  ADD KEY `FK_Notification_User` (`nt_user_code`);

--
-- Indices de la tabla `notification_type`
--
ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`ntt_code`);

--
-- Indices de la tabla `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`plac_code`);

--
-- Indices de la tabla `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`proc_code`);

--
-- Indices de la tabla `productioncenter`
--
ALTER TABLE `productioncenter`
  ADD PRIMARY KEY (`prodcent_code`);

--
-- Indices de la tabla `responsable`
--
ALTER TABLE `responsable`
  ADD PRIMARY KEY (`resp_code`);

--
-- Indices de la tabla `teachingmaterial`
--
ALTER TABLE `teachingmaterial`
  ADD PRIMARY KEY (`teacmate_code`);

--
-- Indices de la tabla `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`them_code`);

--
-- Indices de la tabla `trainerposition`
--
ALTER TABLE `trainerposition`
  ADD PRIMARY KEY (`traiposi_code`);

--
-- Indices de la tabla `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`trai_code`),
  ADD KEY `FK_Training_TrainigState_idx` (`trai_state`),
  ADD KEY `FK_Training_ProductionCenter_idx` (`trai_production_center`),
  ADD KEY `FK_trainig_Process_idx` (`trai_process`),
  ADD KEY `FK_Trainig_Theme_idx` (`trai_theme`),
  ADD KEY `FK_Trainig_Addressed_idx` (`trai_addressed`),
  ADD KEY `FK_Trainig_Responsable_idx` (`trai_responsable`),
  ADD KEY `FK_Trainig_TrainerPosition_idx` (`trai_trainer_position`),
  ADD KEY `FK_training_Place_idx` (`trai_place`);

--
-- Indices de la tabla `trainingstate`
--
ALTER TABLE `trainingstate`
  ADD PRIMARY KEY (`traistat_code`);

--
-- Indices de la tabla `trainingteachingmaterial`
--
ALTER TABLE `trainingteachingmaterial`
  ADD PRIMARY KEY (`traiteacmate_training_code`,`traiteacmate_material_code`),
  ADD KEY `FK_TrainingTeachingMaterial_TeachingMaterial_idx` (`traiteacmate_material_code`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_code`),
  ADD KEY `FK_User_UserType_idx` (`user_type`),
  ADD KEY `FK_User_ProductionCenter_idx` (`user_production_center`);

--
-- Indices de la tabla `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`usertype_code`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `assistant`
--
ALTER TABLE `assistant`
  ADD CONSTRAINT `FK_Assistant_Training` FOREIGN KEY (`assi_training_code`) REFERENCES `training` (`trai_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `audit`
--
ALTER TABLE `audit`
  ADD CONSTRAINT `FK_Audit_AuditType` FOREIGN KEY (`audi_type`) REFERENCES `audittype` (`auditype_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Audit_User` FOREIGN KEY (`audi_user`) REFERENCES `user` (`user_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_Notificacion_Training` FOREIGN KEY (`nt_training_code`) REFERENCES `training` (`trai_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Notification_NotificationType` FOREIGN KEY (`nt_type`) REFERENCES `notification_type` (`ntt_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Notification_User` FOREIGN KEY (`nt_user_code`) REFERENCES `user` (`user_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `FK_Trainig_Addressed` FOREIGN KEY (`trai_addressed`) REFERENCES `addressed` (`addr_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Trainig_Responsable` FOREIGN KEY (`trai_responsable`) REFERENCES `responsable` (`resp_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Trainig_Theme` FOREIGN KEY (`trai_theme`) REFERENCES `theme` (`them_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Trainig_TrainerPosition` FOREIGN KEY (`trai_trainer_position`) REFERENCES `trainerposition` (`traiposi_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Training_ProductionCenter` FOREIGN KEY (`trai_production_center`) REFERENCES `productioncenter` (`prodcent_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_Training_TrainigState` FOREIGN KEY (`trai_state`) REFERENCES `trainingstate` (`traistat_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_trainig_Process` FOREIGN KEY (`trai_process`) REFERENCES `process` (`proc_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_training_Place` FOREIGN KEY (`trai_place`) REFERENCES `place` (`plac_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `trainingteachingmaterial`
--
ALTER TABLE `trainingteachingmaterial`
  ADD CONSTRAINT `FK_TrainingTeachingMaterial_TeachingMaterial` FOREIGN KEY (`traiteacmate_material_code`) REFERENCES `teachingmaterial` (`teacmate_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_TrainingTeachingMaterial_Training` FOREIGN KEY (`traiteacmate_training_code`) REFERENCES `training` (`trai_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_User_ProductionCenter` FOREIGN KEY (`user_production_center`) REFERENCES `productioncenter` (`prodcent_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_User_UserType` FOREIGN KEY (`user_type`) REFERENCES `usertype` (`usertype_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `update_training_state` ON SCHEDULE EVERY 1 DAY STARTS '2018-10-09 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE training
	SET trai_state = 2
    WHERE DATEDIFF(trai_date_format, NOW()) <= 3 AND trai_state = 1$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
