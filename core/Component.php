<?php

/**
 * Clase encarda de cargar componentes html
 */
class Component
{

	public function __construct(string $module, string $subModule)
	{
		/**
		 * Validar si el archivo correspondiente al componente existe
		 */
		if(file_exists(PATH["view"]."{$module}/{$module}-{$subModule}.html")){
			include PATH["view"]."{$module}/{$module}-{$subModule}.html";
		} else{
			endComponent("not-found", "Componente no encontrado.");
		}
	}
}
