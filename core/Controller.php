<?php
/**
 * Clase principal de los controladores
 * Se encarga de el redireccionamiento de las peticiones
 */
class Controller
{

    private $action, $subAction, $rule = [];

    //Carga el modelo de la clase hija llamada
    public function __construct()
    {
        if (method_exists($this, "loadModel")) {
            $this->loadModel();
        }
    }

    //Función que se ejecuta cuando se llama una clase hija por url
    public function runFromUrl(): void
    {
        if (isset($_GET["action"]) && !is_null($_GET["action"]) && $_GET["action"] !== ""){
            $this->action = formatAction($_GET["action"]);
            if (method_exists($this, $this->action)){

                $this->rule = CONTROLLER_MAP["defaultRule"];
                $this->checkRule();
                $this->checkSession();
                $this->execute();

            } else{
                endAjax("error", "Acción no encontrada.");
            }
        } else{
            endAjax("error", "Formato incorrecto.");
        }
    }

    //Carga y valida las reglas definidas en 'bin/Dictionaries/controller-map.json'
    private function checkRule(): void
    {
        $this->controller = get_class($this);
        if (isset(CONTROLLER_MAP[$this->controller][$this->action])) {
            $this->rule = array_merge($this->rule, CONTROLLER_MAP[$this->controller][$this->action]);
        }

        if (!$this->rule["enabled"]) {
            endAjax("error", "Acción no disponible.");
        }
    }

    //Valida si la Función require una sesión
    private function checkSession(): void
    {
        if ($this->rule["session"]) {
            if (Session::check("user")) {
                $user = Session::getByName("user");
                if (!$user->controllerPermission($this->controller, $this->action)) {
                    endAjax("error", "No tienes permisos.");
                }
            } else {
                endAjax("error", "La acción requiere una sesión.");
            }
        }
    }

    //Ejecuta la función despues de pasar por los filtros anteriores
    private function execute(): void
    {
        try {
            $action = $this->action;

            if (method_exists($this, "loadModel")) {
                $this->loadModel();
            }

            //Se valida el tipo de petición
            if ($_GET) {
                if ($_POST) {
                    $validated = GUMP::is_valid($_POST, array(
                    	"data" => "required|valid_json_string"
                    ));

                    if (!$validated) {
                        endAjax("error", "Alto ahi pisllo!");
                    }
                    $data = jsonToArray($_POST["data"]);
                    $result = $this->$action($data);
                } else {
                    $result = $this->$action();
                }

                if (!is_null($result)) {
                    endAjax(
                        $result["status"],
                        $result["msg"],
                        $result["data"]
                    );
                }
            }

        } catch (\Exception $e) {
            endAjax("error", "Desconocido.".$e);
        }
    }
}
