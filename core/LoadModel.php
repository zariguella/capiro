<?php
/**
 * Clase encargada de cargar los archivos correspondientes a los modelos
 */
class LoadModel
{

	public function __construct(string $model)
	{
		if (file_exists(PATH["model"]."{$model}Model.php")) {
			require_once PATH["model"]."{$model}Model.php";
		}else {
			endApp("Modelo no encontrado.");
		}
	}
}
