<?php
namespace Core\App;
/**
 * Clase encargada de cargar las diferentes partes de la aplicación
 */
class App
{

    public static function init(): void
    {
        ob_start();
        //Funciones necesarias para el arranque
        require "bin/Functions/json.php";
        require "bin/Functions/output.php";
    }

    public static function loadConfig(): void
    {
        //Cargamos las configuraciones
        define("APP", jsonToArray("config/app.json", "file"));
        define("PATH", jsonToArray("config/path.json", "file"));
        define("DB", jsonToArray("config/db.json", "file"));

        self::state();
    }

    public static function loadCore(): void
    {

        //Clases principales
        require "core/Router.php";
        require "core/Controller.php";
        require "core/Component.php";
        require "core/Model.php";
        require "core/LoadModel.php";
        require "core/LoadController.php";
        require "core/Layout.php";

        //Funciones principales
        require "bin/Functions/format.php";
        require "bin/Functions/str.php";

        //Diccionarios principales
        define("CONTROLLER_MAP", jsonToArray("bin/Dictionaries/controller-map.json", "file"));
        define("VIEW_MAP", jsonToArray("bin/Dictionaries/view-map.json", "file"));

        //Clases del sistema
        require "bin/SystemClass/User.php";
        require "bin/SystemClass/Session.php";
        require "bin/SystemClass/Audit.php";
        require "bin/SystemClass/Cookie.php";

        //Conexión a otros sistemas
        require "bin/Connections/ActiveDirectoryConn.php";
        require "bin/Connections/RosterConn.php";

        //Librerias
        require "vendor/autoload.php";

        ob_end_clean();
    }

    public static function end(): void
    {
        exit;
    }

    public static function state(): void
    {
        //Verifica el estado de la aplicación
        if (APP["state"] === "maintenance") {
            exit("aplicacion en mantenimiento");
        }
    }
}
