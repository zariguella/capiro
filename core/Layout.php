<?php
/**
 * Clase encargada de cargar las diferentes partes de la vista
 */
class Layout
{

	public function __construct(
		string $module, string $subModule, bool $allLibraries = true, bool $navbar = false,
		bool $alert = false, bool $navigationBar = true,
		bool $content = true, string $ext = "html"
		)
	{
		//Se valida la existencia del archivo 'modulo' que se debe cargar
		if (file_exists(PATH["view"]."{$module}/{$module}-{$subModule}.{$ext}")){

			//Se valdia la existencia del archivo 'cabeza'
			if (file_exists(PATH["view"].PATH["head"])) {
				include PATH["view"].PATH["head"];
			}else {
				endApp("Encabezado no encontrado.");
			}

			//Se valida la existencia del archivo de las alertas
			if ($alert) {
				if (file_exists(PATH["view"].PATH["alert"])) {
					include PATH["view"].PATH["alert"];
				}else {
					endApp("Sistema de alertas no encontrado.");
				}
			}

			//Se valida la existencia del archivo de la barra de navegación
			if ($navbar) {
				if (file_exists(PATH["view"].PATH["navbar"])) {
					include PATH["view"].PATH["navbar"];
				}else {
					endApp("Barra de navegación no encontrada.");
				}
			}

			if ($content) {
				include PATH["view"].PATH["startBody"];
			}

			include PATH["view"]."{$module}/{$module}-{$subModule}.{$ext}";

			if ($content) {
				include PATH["view"].PATH["endBody"];
			}

			//Se valida la existencia del 'pie de pagina'
			if (file_exists(PATH["view"].PATH["footer"])) {
				include PATH["view"].PATH["footer"];
			}else {
				endApp("Pie de página no encontrado.");
			}
		} else{
			redirTo("error/404");
		}
	}
}
