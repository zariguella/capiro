<?php
/**
 * Clase encargada de cargar los archivos correspondientes a los controladores
 */
class LoadController
{

	public function __construct(string $controller)
	{
		if (file_exists(PATH["controller"]."{$controller}Controller.php")) {
			require_once PATH["controller"]."{$controller}Controller.php";
		}else {
			endApp("Controlador no encontrado.");
		}
	}
}
