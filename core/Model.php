 <?php
/**
 * Clase encargada de la conexión a la base de datos principal
 */
class Model {

 	public $conn = null;

 	public function __construct()
    {
		try {
			$this->conn = $this->getConnection();
		} catch(PDOException $ex) {
			endAjax("error", "No se pudo conectar a la base de datos.".$ex);
		}
	}

    /**
     * Se obtiene la conexión
     * @return PDO [Conexión a la DB]
     */
	private function getConnection()
    {

        $DB = DB["Training"];

		$opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];

        $dsd = "{$DB['driver']}:host={$DB['server']};dbname={$DB['database']};charset={$DB['charset']}";
		$pdo = new pdo($dsd, $DB['user'], $DB['pass'], $opt);

		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
	}

    //Crea cadenas de texto, con formato de insertar desde un array
    protected function getStrForCreate(array $data): array
    {
        $count = 0;
        $fields = "";
        $params = "";
        foreach ($data as $key => $value) {
            if ($count !== 0) {
                $fields = $fields.", ";
                $params = $params.", ";
            }
            $fields = $fields.$key;
            $params = $params.":".$key;
            $count++;
        }
        return $string = ["fields" => $fields, "params" => $params];
    }

    //Crea cadenas de texto, con formato de actualizar desde un array
    protected function getStrForUpdate(array $data): string
    {
        $count = 0;
        $string = "";
        foreach ($data as $key => $value) {
            if ($count !== 0) {
                $string = $string.", ";
            }
            $string = $string.$key."=:".$key;
            $count++;
        }
        return $string;
    }

    //Cambia los nombres de las claves de un array con el nombre de los campos de la base de datos
    protected function linkParams(array &$data, array $params): void
    {
        $newData = [];
        foreach ($params as $key => $value) {
            if (isset($data[$key])) {
                $newData[$params[$key]] = $data[$key];
            }
        }
        $data = $newData;
    }
    //Vincula solo una clave de un array con el nombre de un campo de la base de datos
    protected function linkParam(string &$by, array $params): void
    {
        $newBy = "";
        foreach ($params as $key => $value) {
            if ($by == $key) {
                $newBy = $params[$key];
            }
        }
        $by = $newBy;
    }
 }
