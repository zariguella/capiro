<?php
namespace Core\Router;
/**
 * Clase encargada de cargar de verificar y validar la url, en busca de un controlador,
 * cargando el archivo correspondiente y retornando el nombre del controlador formateado
 */
class Router
{
    private $controller;

    public function __construct()
    {
        if ($_GET && isset($_GET["controller"])){
            if (!(is_null($_GET["controller"])) && is_string($_GET["controller"]) && !(empty($_GET["controller"]))){
                $this->controller = formatController($_GET["controller"]);

                if (file_exists(PATH["controller"].$this->controller."Controller.php")){
                    require(PATH["controller"].$this->controller."Controller.php");
                } else{
                    endApp("Controlador no encontrado (Router).");
                }
            } else{
                endApp("Formato incorrecto del controlador.");
            }
        } else{
            endApp("Controlador no encontrado (Router).");
        }
    }

    public function getController(): string
    {
        return $this->controller."Controller";
    }

    public function __destruct(){}

}
