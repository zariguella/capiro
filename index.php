<?php
//---------------------------------------------
//usleep(4 * 100000);
//---------------------------------------------
//Requiere el archivo principal de la apliación.
require "core/App.php";

//Se usan los namespaces
use Core\App\App;
use Core\Router\Router;

//Se hace referencia a metodos estaticos, encargados de cargar diferentes archivos y configuraciones
App::init();
App::loadConfig();
App::loadCore();
Session::start();

//Se obtiene el controlador llamado por urlz
$controller = (new Router())->getController();

//Se crear un controlador y ejecuta la función llamada por url
(new $controller)->runFromUrl();

//Finaliza la aplicación
App::end();
